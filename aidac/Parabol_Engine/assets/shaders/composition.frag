#version 450

#define AMBIENT 0.2f
#define EXPOSURE 1.0f
#define GAMMA 2.7f

layout (input_attachment_index = 0, binding = 0) uniform subpassInput Albedo;
layout (input_attachment_index = 1, binding = 1) uniform subpassInput Position;
layout (input_attachment_index = 2, binding = 2) uniform subpassInput Normal;

struct LightSource {
	vec3 position;
	float luminosity;
	vec4 color;
};

layout (constant_id = 0) const int NUM_LIGHTS = 1;
layout (std140, binding = 3) uniform UniformBufferObject {
    LightSource lightSources[NUM_LIGHTS];
} ubo;

layout (location = 0) out vec4 oColor;

void main()
{
	float diffuse = AMBIENT;
	for (int l = 0; l < NUM_LIGHTS; l++) {
		vec3 lightVector = ubo.lightSources[l].position - subpassLoad(Position).xyz;
		float brightness = ubo.lightSources[l].luminosity * exp(-length(lightVector)); // exponential attenuation
		diffuse += brightness * clamp(0.0f, 1.0f, dot(normalize(lightVector), subpassLoad(Normal).xyz));
	}

	vec3 color = diffuse * subpassLoad(Albedo).rgb;
	//color = 1.0f - exp(color * EXPOSURE); // exposure tone mapping
	//color = pow(color, vec3(1.0f / GAMMA)); // gamma correction
	oColor = vec4(color, 1.0f);
}