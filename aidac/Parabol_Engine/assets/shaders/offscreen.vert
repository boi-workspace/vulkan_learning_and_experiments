#version 450

// 2) Vertex Shader
// Applies transformations to turn vertex positions from model space to screen space

// vertex attributes
layout (location = 0) in vec3 iPosition;
layout (location = 1) in vec3 iColor;
layout (location = 2) in vec2 iUV;
layout (location = 3) in vec3 iNormal;

// Instanced attributes
layout (location = 4) in vec4 iModel1;
layout (location = 5) in vec4 iModel2;
layout (location = 6) in vec4 iModel3;
layout (location = 7) in vec4 iModel4;

// output
layout (location = 0) out vec2 oUV;
layout (location = 1) out vec3 oPosition;
layout (location = 2) out vec3 oNormal;

layout (binding = 0) uniform UniformBufferObject {
    mat4 view;
    mat4 proj;
} ubo;

void main() {
	mat4 model = mat4(iModel1, iModel2, iModel3, iModel4);
	vec4 worldPosition = model * vec4(iPosition, 1.0);

	gl_Position = ubo.proj * ubo.view * worldPosition;
	oUV = iUV;
	oPosition = worldPosition.xyz;
	oNormal = iNormal;
}