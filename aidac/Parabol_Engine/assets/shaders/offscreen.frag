#version 450

// 6) Fragment Shader
// For each fragment, produce a color and depth for the framebuffer

layout (set = 0, binding = 1) uniform texture2D tex;
layout (set = 0, binding = 2) uniform sampler samp;

layout (location = 0) in vec2 iUV;
layout (location = 1) in vec3 iPosition;
layout (location = 2) in vec3 iNormal;

layout (location = 0) out vec4 oAlbedo;
layout (location = 1) out vec4 oPosition;
layout (location = 2) out vec4 oNormal;

void main() {
	oAlbedo = texture(sampler2D(tex, samp), iUV);
	oPosition = vec4(iPosition, 1.0f);
	oNormal = vec4(normalize(iNormal), 0.0f);
}