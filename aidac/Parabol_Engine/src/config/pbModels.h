#pragma once

// TODO: this shouldn't be a source file. convert into text map file

#include <glm/glm.hpp>
#include <string>
#include <array>

// !THESE VALUES CANNOT BE LESS THAN 1! //
#define NUM_LIGHTS 3
#define NUM_MODELS 2

namespace pbModels {
	struct LightSource {
		glm::vec3 position;
		float luminosity;
		glm::vec4 color;
	};

	const std::array<LightSource, NUM_LIGHTS> lightSources = {{
		{ glm::vec3(0, 0, 3), 4.0, glm::vec4(1.0, 1.0, 0.9, 1) },
		{ glm::vec3(0, 3,-1), 4.0, glm::vec4(1.0, 0.7, 0.7, 1) },
		{ glm::vec3(2, 1,-1), 4.0, glm::vec4(0.1, 0.4, 0.5, 1) }
	}};

	struct modelConfig {
		std::string name;
		std::vector<glm::vec3> instancePositions;
	};

	const std::array<modelConfig, NUM_MODELS> modelConfigs = {{
		{ "donut_tutorial", { glm::vec3(0, 0, 0), glm::vec3(4, 0, 0), glm::vec3(0, 4, 0), glm::vec3(4, 4, 0) } },
		{ "chalet", { glm::vec3(0,0,-3) } }
	}};
}