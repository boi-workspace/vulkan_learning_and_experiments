#pragma once

#include "renderer/Renderer.h"
#include "interface/Interface.h"

#include <glm/glm.hpp> // vector/matrix linear algebra
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#include <vector>
#include <chrono> // precise timekeeping
using namespace std::chrono;

int main();

	// ---------- PARABOL ENGINE CLASS ---------- //

class Parabol {
public:
	void Run();

	static void windowResizeCallback(GLFWwindow *window, int width, int height);

private:
	Renderer pbRenderer;
	Interface pbInterface;

	uint32_t keyInputs = 0;
	uint32_t keyInputsPrev = 0;

	bool windowResized = false;

	float strafeSpeed = 1.5;
	float forwardSpeed = 2;
	float backSpeed = 1;
	float radiansPerMousePosHoriz = -0.0005f;
	float radiansPerMousePosVert = 0.0005f;
	float radiansPerSecondFront = (float)M_PI / 4;
	time_point<high_resolution_clock> timePrev; // time value from the previous render

	glm::vec3 viewerPosition	= glm::vec3(-3.0f, 0.0f, 0.0f); // your position in the world
	glm::vec3 viewerDirection	= glm::vec3(1.0f, 0.0f, 0.0f); // direction you are facing
	glm::vec3 viewerUp			= glm::vec3(0.0f, 0.0f, 1.0f); // viewer up direction
	glm::vec3 viewerCross		= glm::cross(viewerUp, viewerDirection); // cross product up x direction

	void pbInit();
	void pbLoop();
	void pbCleanup();

	void pbProcessInputs();
	void printViewData();
};