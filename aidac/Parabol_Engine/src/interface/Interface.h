#pragma once

#include "config/pbInputs.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h> // TODO put in cpp?
#include <map>

class Parabol;

class Interface {
public:

	void Init(Parabol *application);
	void Cleanup();

	void MinimizeCheck();
	VkResult CreateWindowSurface(VkInstance& instance, const VkAllocationCallbacks* allocator, VkSurfaceKHR* surface);
	void GetFramebufferSize(int* width, int* height);
	int WindowCloseCheck();
	inline void PollEvents() { glfwPollEvents(); };

	unsigned int GetKeyInputs();
	void GetMouseChange(double *mouseX, double *mouseY);

private:

	void setKeyBindings();
	std::map<int, PB_INPUTS> keyBindings;
	GLFWwindow *window;
	double mousePosPrev[2];
};

