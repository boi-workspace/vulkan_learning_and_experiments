#include "Interface.h"
#include "logging/Log.h"
#include "Parabol.h"

const int WIDTH = 1000;
const int HEIGHT = 700;

// ---------- PUBLIC FUNCTIONS ---------- //

void Interface::Init(Parabol *application) {

	glfwInit(); // initalizes the glfw library
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // tells GLFW not to make an OpenGL context

	window = glfwCreateWindow(WIDTH, HEIGHT, "Seagull", nullptr, nullptr); // make a window
	glfwSetWindowUserPointer(window, application);
	glfwSetFramebufferSizeCallback(window, application->windowResizeCallback);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // disable mouse cursor
	glfwGetCursorPos(window, &mousePosPrev[0], &mousePosPrev[1]); // get mouse position

	// set key bindings
	setKeyBindings();
}

void Interface::Cleanup() {

	// destroy the window object
	glfwDestroyWindow(window);
	glfwTerminate();
}

void Interface::MinimizeCheck() {
	int width = 0, height = 0;
	while (width == 0 || height == 0) {
		glfwGetFramebufferSize(window, &width, &height);
		glfwWaitEvents();
	}
}

VkResult Interface::CreateWindowSurface(VkInstance& instance, const VkAllocationCallbacks* allocator, VkSurfaceKHR* surface) {
	return glfwCreateWindowSurface(instance, window, allocator, surface);
}

void Interface::GetFramebufferSize(int* width, int* height) {
	glfwGetFramebufferSize(window, width, height);
}

int Interface::WindowCloseCheck() {
	return glfwWindowShouldClose(window);
}

void Interface::setKeyBindings() {
	keyBindings[GLFW_KEY_ESCAPE] = PB_INPUTS::ESC;

	keyBindings[GLFW_KEY_W] = PB_INPUTS::UP;
	keyBindings[GLFW_KEY_A] = PB_INPUTS::LEFT;
	keyBindings[GLFW_KEY_S] = PB_INPUTS::DOWN;
	keyBindings[GLFW_KEY_D] = PB_INPUTS::RIGHT;
	keyBindings[GLFW_KEY_Q] = PB_INPUTS::ROTATEL;
	keyBindings[GLFW_KEY_E] = PB_INPUTS::ROTATER;

	keyBindings[GLFW_KEY_UP] = PB_INPUTS::UP;
	keyBindings[GLFW_KEY_DOWN] = PB_INPUTS::DOWN;
	keyBindings[GLFW_KEY_LEFT] = PB_INPUTS::LEFT;
	keyBindings[GLFW_KEY_RIGHT] = PB_INPUTS::RIGHT;
	keyBindings[GLFW_KEY_PAGE_UP] = PB_INPUTS::ROTATEL;
	keyBindings[GLFW_KEY_PAGE_DOWN] = PB_INPUTS::ROTATER;

	keyBindings[GLFW_KEY_SPACE] = PB_INPUTS::FORWARD;
	keyBindings[GLFW_KEY_LEFT_SHIFT] = PB_INPUTS::BACKWARD;

	keyBindings[GLFW_KEY_Z] = PB_INPUTS::INTERACTL;
	keyBindings[GLFW_KEY_X] = PB_INPUTS::INTERACTR;
}

// returns pbInputs flags depending on wherever keyBindings keys are being pressed
uint32_t Interface::GetKeyInputs() {
	uint32_t inputs = 0;

	for (auto&[key, input] : keyBindings) {
		int state = glfwGetKey(window, key);
		if (state == GLFW_PRESS)
			inputs |= input;
	}

	return inputs;
}

void Interface::GetMouseChange(double *mouseX, double *mouseY) {
	double mousePosCurrent[2];
	glfwGetCursorPos(window, &mousePosCurrent[0], &mousePosCurrent[1]); // get current mouse position
	*mouseX = mousePosCurrent[0] - mousePosPrev[0];
	*mouseY = mousePosCurrent[1] - mousePosPrev[1];
	glfwGetCursorPos(window, &mousePosPrev[0], &mousePosPrev[1]); // set previous position
}