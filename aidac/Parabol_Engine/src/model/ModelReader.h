#pragma once
/*
Reads in a model that has been converted to binary by the Model_Loader project
!! Must be updated to match the Model_Loader format !!
*/

#include "version_number.h"
#include "Vertex.h"
#include "logging\Log.h"

#include <fstream>
#include <string>

namespace ModelReader {

	bool fileExists(const std::string& name) {
		struct stat buffer;
		return (stat(name.c_str(), &buffer) == 0);
	}

	void read(std::string filepath, std::vector<uint32_t> &indices, std::vector<Vertex> &vertices) {
		if (!fileExists(filepath)) {
			PB_ERROR("model reader: file not found: {}", filepath);
			throw std::runtime_error("~ model reader file not found");
		}

		// open
		std::ifstream input(filepath, std::ios::binary);

		// read version number
		uint16_t *versionBuffer = new uint16_t[1];
		input.read(reinterpret_cast<char*>(versionBuffer), sizeof(uint16_t));
		if (*versionBuffer != (uint16_t)MODEL_VERSION_NUMBER) {
			throw std::runtime_error("~ MODEL READER ERROR: incompatible model version for file: " + filepath);
			return;
		}

		// read data size
		uint32_t *sizeBuffer = new uint32_t[1];
		input.read(reinterpret_cast<char*>(sizeBuffer), sizeof(uint32_t));
		uint32_t num_vertices = *sizeBuffer;
		input.read(reinterpret_cast<char*>(sizeBuffer), sizeof(uint32_t));
		uint32_t num_indices = *sizeBuffer;

		// read model data
		Vertex *vertBuffer = new Vertex[num_vertices];
		input.read(reinterpret_cast<char*>(vertBuffer), sizeof(Vertex)*num_vertices);
		uint32_t *indBuffer = new uint32_t[num_indices];
		input.read(reinterpret_cast<char*>(indBuffer), sizeof(uint32_t)*num_indices);

		input.close();

		// save model data
		for (int i = 0; i < num_indices; i++) {
			indices.push_back(indBuffer[i]);
		}
		for (int i = 0; i < num_vertices; i++) {
			vertices.push_back(vertBuffer[i]);
		}
	}
}