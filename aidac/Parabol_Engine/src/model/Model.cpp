#include "Model.h"
#include "ModelReader.h"
#include "logging/Log.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp> // hash funtions for the glm types

void Model::LoadMesh(std::string name) {
	//ModelReader::read(MODEL_PATH + name + ".bin", indices, vertices);
	PB_INFO("Loaded model: " + name);
}

void Model::CleanUp(VkDevice logicalDevice) {
	vkDestroyImageView(logicalDevice, texture.imageView, nullptr);
	vkDestroyImage(logicalDevice, texture.image, nullptr);
	vkFreeMemory(logicalDevice, texture.memory, nullptr);
}

std::vector <VkVertexInputBindingDescription> Model::getBindingDescriptions() {
	std::vector <VkVertexInputBindingDescription> bindingDescription = {};
	bindingDescription.resize(2);

	// Binding point 0: Mesh vertex layout description at per-vertex rate
	bindingDescription[0].binding = 0;
	bindingDescription[0].stride = sizeof(Vertex);
	bindingDescription[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	// Binding point 1: Instanced data at per-instance rate
	bindingDescription[1].binding = 1;
	bindingDescription[1].stride = sizeof(ModelMatrix);
	bindingDescription[1].inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;
	return bindingDescription;
}

std::vector<VkVertexInputAttributeDescription> Model::getAttributeDescriptions() {
	std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
	attributeDescriptions.resize(8);

	// per-vertex attribbutes (binding 0)
	// position
	attributeDescriptions[0].binding = 0; // which binding the data comes from
	attributeDescriptions[0].location = 0; // corresponds to location in vertex shader
	attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
	attributeDescriptions[0].offset = offsetof(Vertex, pos);
	// color
	attributeDescriptions[1].binding = 0;
	attributeDescriptions[1].location = 1;
	attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
	attributeDescriptions[1].offset = offsetof(Vertex, color);
	// texture coordinates
	attributeDescriptions[2].binding = 0;
	attributeDescriptions[2].location = 2;
	attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
	attributeDescriptions[2].offset = offsetof(Vertex, texCoord);
	// normals
	attributeDescriptions[3].binding = 0;
	attributeDescriptions[3].location = 1;
	attributeDescriptions[3].format = VK_FORMAT_R32G32B32_SFLOAT;
	attributeDescriptions[3].offset = offsetof(Vertex, normal);

	// per-instance attributes (binding 1)
	// model matrix row 1
	attributeDescriptions[4].binding = 1;
	attributeDescriptions[4].location = 4;
	attributeDescriptions[4].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[4].offset = 0;
	// model matrix row 2
	attributeDescriptions[5].binding = 1;
	attributeDescriptions[5].location = 5;
	attributeDescriptions[5].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[5].offset = sizeof(float) * 4;
	// model matrix row 3
	attributeDescriptions[6].binding = 1;
	attributeDescriptions[6].location = 6;
	attributeDescriptions[6].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[6].offset = sizeof(float) * 8;
	// model matrix row 4
	attributeDescriptions[7].binding = 1;
	attributeDescriptions[7].location = 7;
	attributeDescriptions[7].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[7].offset = sizeof(float) * 12;
	return attributeDescriptions;
}
