#pragma once

#include "Object.h"
#include <string>

class Model : Object
{
public:
	struct Vertex;
	void LoadMesh(std::string name);
	void CleanUp(VkDevice device);

	std::vector<VkVertexInputBindingDescription> getBindingDescriptions();
	std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();

private:
	struct {
		VkImage image;
		VkImageView imageView; // TODO what is the view for?
		VkDeviceMemory memory;
	} texture;
};

struct Model::Vertex {
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec2 texCoord;

	bool operator==(const Vertex& other) const {
		return pos == other.pos && normal == other.normal && texCoord == other.texCoord;
	}
};
