#include "LightSource.h"
#include "logging/Log.h"

void LightSource::LoadMesh(std::string name) {
	//ModelReader::read(MODEL_PATH + name + ".bin", indices, vertices);
	PB_INFO("Loaded model: " + name);
}

void LightSource::CleanUp(VkDevice device) {}

std::vector <VkVertexInputBindingDescription> LightSource::getBindingDescriptions() {
	std::vector <VkVertexInputBindingDescription> bindingDescription = {};
	bindingDescription.resize(2);

	// Binding point 0: Mesh vertex layout description at per-vertex rate
	bindingDescription[0].binding = 0;
	bindingDescription[0].stride = sizeof(Vertex);
	bindingDescription[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	// Binding point 1: Instanced data at per-instance rate
	bindingDescription[1].binding = 1;
	bindingDescription[1].stride = sizeof(ModelMatrix);
	bindingDescription[1].inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;
	return bindingDescription;
}

std::vector<VkVertexInputAttributeDescription> LightSource::getAttributeDescriptions() {
	std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
	attributeDescriptions.resize(6);

	// per-vertex attribbutes (binding 0)
	// position
	attributeDescriptions[0].binding = 0; // which binding the data comes from
	attributeDescriptions[0].location = 0; // corresponds to location in vertex shader
	attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
	attributeDescriptions[0].offset = offsetof(Vertex, pos);
	// normals
	attributeDescriptions[1].binding = 0;
	attributeDescriptions[1].location = 1;
	attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
	attributeDescriptions[1].offset = offsetof(Vertex, normal);

	// per-instance attributes (binding 1)
	// model matrix row 1
	attributeDescriptions[2].binding = 1;
	attributeDescriptions[2].location = 4;
	attributeDescriptions[2].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[2].offset = 0;
	// model matrix row 2
	attributeDescriptions[3].binding = 1;
	attributeDescriptions[3].location = 5;
	attributeDescriptions[3].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[3].offset = sizeof(float) * 4;
	// model matrix row 3
	attributeDescriptions[4].binding = 1;
	attributeDescriptions[4].location = 6;
	attributeDescriptions[4].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[4].offset = sizeof(float) * 8;
	// model matrix row 4
	attributeDescriptions[5].binding = 1;
	attributeDescriptions[5].location = 7;
	attributeDescriptions[5].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[5].offset = sizeof(float) * 12;
	return attributeDescriptions;
}