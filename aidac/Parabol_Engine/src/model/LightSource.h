#pragma once

#include "Object.h"

class LightSource : Object
{
public:
	struct Vertex;
	void LoadMesh(std::string name);
	void CleanUp(VkDevice device);

	void Update(double time);

	std::vector<VkVertexInputBindingDescription> getBindingDescriptions();
	std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();

private:
	glm::vec3 color = glm::vec3(1.0f);
	float luminescence = 1.0f;
};

struct LightSource::Vertex {
	glm::vec3 pos;
	glm::vec3 normal;

	bool operator==(const Vertex& other) const {
		return pos == other.pos && normal == other.normal;
	}
};
