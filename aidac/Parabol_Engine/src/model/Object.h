#pragma once

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#include <vector>
#include <string>

#define MODEL_PATH "assets/models/"

struct ModelMatrix {
	glm::vec4 model1;
	glm::vec4 model2;
	glm::vec4 model3;
	glm::vec4 model4;
};

// not used for anything?
class Object
{
public:
	std::string name = "";
	struct Vertex;

	virtual void LoadMesh(std::string name) = 0;
	virtual void CleanUp(VkDevice device) = 0;
	virtual std::vector<VkVertexInputBindingDescription> getBindingDescriptions() = 0;
	virtual std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions() = 0;

protected:
	std::vector<uint32_t> indices;
	std::vector<Vertex> vertices;
	std::vector<ModelMatrix> instances;

	// helper functions
	bool fileExists(const std::string& name) {
		struct stat buffer;
		return (stat(name.c_str(), &buffer) == 0);
	}
};
