
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h> // image reading library

#define GLM_FORCE_DEPTH_ZERO_TO_ONE // ensures perspective projection matrix uses vulkan range [0,1]
#define GLM_FORCE_RADIANS // ensures glm functions use radians
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/rotate_vector.hpp>

#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <functional>
#include <algorithm>
#include <fstream>

#include "Renderer.h"
#include "logging/Log.h"

// ---------- CONSTS ---------- //

const std::string OFFSCREEN_VERT_PATH = "assets/shaders/spv/offscreen_vert.spv";
const std::string OFFSCREEN_FRAG_PATH = "assets/shaders/spv/offscreen_frag.spv";

const std::string COMPOSITION_VERT_PATH = "assets/shaders/spv/composition_vert.spv";
const std::string COMPOSITION_FRAG_PATH = "assets/shaders/spv/composition_frag.spv";

const std::vector<const char*> validationLayers = {
	"VK_LAYER_LUNARG_standard_validation",
	"VK_LAYER_LUNARG_assistant_layer",
	"VK_LAYER_LUNARG_monitor"
};

const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

// if app not compiled in debug mode
#ifdef PB_DEBUG
const bool enableValidationLayers = true;
// added extensions: VK_EXT_debug_utils
#else
const bool enableValidationLayers = false;
#endif

// ---------- VULKAN EXTENSION FUNCTIONS ---------- //

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	if (func != nullptr) {
		return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
	} else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}
void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if (func != nullptr) {
		func(instance, debugMessenger, pAllocator);
	}
}

// ---------- VULKAN RENDERER ---------- //

void Renderer::Init(Interface *pbInterface) {
	this->pbInterface = pbInterface;

	createInstance(); // the instance is the connection between application and the vulkan library
	setupDebugMessenger(); // set up debug report funcionality (if in debug mode)
	createSurface();
	pickPhysicalDevice();
	createLogicalDevice();
	createSwapChain();
	createImageViews();
	createRenderPass();
	createDescriptorSetLayout();
	createGraphicsPipelines();
	createCommandPool();
	createDepthResources();
	createFramebuffers();
	loadModels();
	createTextureImages();
	createTextureImageViews();
	createTextureSampler();
	createGraphicsBuffers();
	createUniformBuffers();
	createDescriptorPool();
	createDescriptorSets();
	createCommandBuffers();
	createSyncObjects();
}

void Renderer::Cleanup() {
	// ensure logical device has finished operations before we start destroying stuff
	vkDeviceWaitIdle(device);

	cleanupSwapChain(); // swap chain objects
	vkDestroySampler(device, offscreen.sampler, nullptr); // texture sampler
	vkDestroyDescriptorPool(device, descriptorPool, nullptr); // descriptor pool
	vkDestroyDescriptorSetLayout(device, offscreen.descriptorSetLayout, nullptr); // descriptor set layout
	vkDestroyDescriptorSetLayout(device, composition.descriptorSetLayout, nullptr); // descriptor set layout
	instanceBuffer.Destroy(device);
	indexBuffer.Destroy(device);
	vertexBuffer.Destroy(device);
	offscreen.UBO.Destroy(device);
	composition.UBO.Destroy(device);
	for (int i = 0; i < NUM_MODELS; i++) {
		models[i].CleanUp(device); // texture image, vertex buffer, index buffer
	}
	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr); // semaphores
		vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
		vkDestroyFence(device, inFlightFences[i], nullptr); // fences
	}
	vkDestroyCommandPool(device, commandPool, nullptr); // command pool
	vkDestroyDevice(device, nullptr); // logical device
	if (enableValidationLayers)
		DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr); // debug utils
	vkDestroySurfaceKHR(instance, surface, nullptr); // surface

	vkDestroyInstance(instance, nullptr);
	PB_INFO("renderer instance destroyed");
}

// seperate function so the swap chain can be recreated
void Renderer::cleanupSwapChain() {
	albedoAttachment.destroy(device);
	positionAttachment.destroy(device);
	normalAttachment.destroy(device);
	depthAttachment.destroy(device);

	for (auto framebuffer : swapChainFramebuffers) {
		vkDestroyFramebuffer(device, framebuffer, nullptr); // framebuffers
	}
	vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data()); // reuse the command pool but allocate new buffers
	vkDestroyPipeline(device, offscreen.pipeline, nullptr); // graphics pipeline
	vkDestroyPipeline(device, composition.pipeline, nullptr); // graphics pipeline
	vkDestroyPipelineLayout(device, offscreen.pipelineLayout, nullptr); // shader pipeline layout
	vkDestroyPipelineLayout(device, composition.pipelineLayout, nullptr); // shader pipeline layout

	vkDestroyRenderPass(device, renderPass, nullptr); // render pass
	for (auto imageView : swapChainImageViews) {
		vkDestroyImageView(device, imageView, nullptr); // image views
	}
	vkDestroySwapchainKHR(device, swapChain, nullptr); // swap chain
}

// ---------- CREATE INSTANCE ---------- //

void Renderer::createInstance() {
	// checks if all requested validation layers are available
	if (enableValidationLayers && !checkValidationLayerSupport()) {
		throw std::runtime_error("~ validation layers requested, but not available! (try running not in debug mode)");
	}

	// fill in this struct with info about our app, allows driver to optimise (optional)
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Hello Triangle";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "No Engine";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_0;

	// this struct tells vulkan global extensions and validation layers we want (compulsary)
	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	// ask glfw what global extensions it needs, then tell Vulkan
	auto extensions = getRequiredExtensions();
	createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	createInfo.ppEnabledExtensionNames = extensions.data();
	// print available extensions
	// extensionsCheck();

	// which global validation layers to enable
	// You can simply enable validation layers for debug builds and completely disable them for release builds, which gives you the best of both worlds!
	// ***Using the validation layers is the best way to avoid your application breaking on different drivers by accidentally relying on undefined behavior.
	if (enableValidationLayers) {
		createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		createInfo.ppEnabledLayerNames = validationLayers.data();
	} else {
		createInfo.enabledLayerCount = 0;
	}

	// Vulkan instance creator funtion
	VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);
	if (result != VK_SUCCESS) {
		PB_ERROR("VkResult return value: {}", result);
		throw std::runtime_error("~ failed to create instance!");
	}
}

// returns a vector of extensions required (by glfw and our debug preferences)
std::vector<const char*> Renderer::getRequiredExtensions() {
	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions;
	glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	// copy all elements in glfwExtensions to a vector
	std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

	if (enableValidationLayers)
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

	return extensions;
}

// print available extensions
void Renderer::extensionsCheck() {
	std::cout << "\n" << "-- Extensions check --" << std::endl;

	// checking for optional Vulkan extensions
	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr); // how many extensions are there?
	std::vector<VkExtensionProperties> extensions(extensionCount);
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data()); // query extension details
	// print available extensions (VkExtensionProperties contains name and version)
	std::cout << "\n" << "available extensions:" << std::endl;
	for (const auto &extension : extensions) {
		std::cout << "\t" << extension.extensionName << std::endl;
	}

	// confirm if all required glfw extensions are available
	uint32_t glfwExtensionCount = 0;
	const char **glfwExtensionNames = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
	std::cout << "\n" << "glfw required extensions:" << std::endl;
	for (int i = 0; i < sizeof(glfwExtensionNames) / sizeof(glfwExtensionNames[0]); i++) {
		std::cout << "\t" << glfwExtensionNames[i];
		bool found = false;
		for (const auto &extension : extensions) {
			if (strcmp(glfwExtensionNames[i], extension.extensionName))
				found = true;
		}
		if (found)
			std::cout << " - available!" << std::endl;
		else
			std::cout << " - not available!" << std::endl;
	}
	std::cout << std::endl;
}

// ---------- VALIDATION LAYERS AND DEBUG CALLBACK ---------- //

// checks if all requested validation layers are available
bool Renderer::checkValidationLayerSupport() {
	// get list of available layers
	uint32_t layerCount;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
	std::vector<VkLayerProperties> availableLayers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

	// check if each specified validation layer name is in availableLayers
	for (const char *layerName : validationLayers) {
		bool layerFound = false;
		for (const auto &layerProperties : availableLayers) {
			if (strcmp(layerName, layerProperties.layerName) == 0) {
				layerFound = true;
				break;
			}
		}
		if (!layerFound)
			return false;
	}
	return true;
}

// set up debug util funcionality (if in debug mode)
void Renderer::setupDebugMessenger() {
	if (!enableValidationLayers) return;

	// ***for more validation layer settings edit vk_layer_settings.txt
	VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
		| VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT; // VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT
	createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
		| VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	createInfo.pfnUserCallback = debugCallbackHandle;

	// create DebugReportCallback object and link it to the instance
	if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS)
		throw std::runtime_error("~ failed to set up debug callback!");
}

// called by the debug report extension to handle error information produced by active layers
VKAPI_ATTR VkBool32 VKAPI_CALL Renderer::debugCallbackHandle(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {
	std::cerr << "~ PB validation layer - " << pCallbackData->pMessage << std::endl;
	return VK_FALSE; // so we don't abort call (only used for testing of layers)
}

// ---------- WINDOW SURFACE ---------- //

// use glfw to create a surface (windows, linux and macOS)
void Renderer::createSurface() {
	// the surface allows us to put stuff in the window
	if (pbInterface->CreateWindowSurface(instance, nullptr, &surface) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create window surface!");
}

// ---------- PHYSICAL DEVICES AND QUEUE FAMILIES ---------- //

void Renderer::pickPhysicalDevice() {
	// get list of devices
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

	if (deviceCount == 0)
		throw std::runtime_error("~ failed to find GPUs with Vulkan support!");

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

	// find first device that meets requirements
	for (const auto &device : devices) {
		if (isDeviceSuitable(device)) {
			physicalDevice = device;
			break;
		}
	}
	if (physicalDevice == VK_NULL_HANDLE)
		throw std::runtime_error("~ failed to find a suitable GPU!");
}

bool Renderer::isDeviceSuitable(VkPhysicalDevice device) {
	// TODO: choose best GPU (section: Physical devices and queue families)
	QueueFamilyIndices indices = findQueueFamilies(device);

	// device extensions (includes swap chain extension)
	bool extensionsSupported = checkDeviceExtensionSupport(device);

	// verify adequate swap chain support
	bool swapChainAdequate = false;
	if (extensionsSupported) {
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}

	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties(device, &physicalDeviceProperties);

	VkPhysicalDeviceFeatures supportedFeatures;
	vkGetPhysicalDeviceFeatures(device, &supportedFeatures);

	// true if a valid queue family was found
	return indices.isComplete() && extensionsSupported && swapChainAdequate && supportedFeatures.samplerAnisotropy;
}

// analyse available queue families
QueueFamilyIndices Renderer::findQueueFamilies(VkPhysicalDevice device) {
	QueueFamilyIndices indices;

	// get list of queue families
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	// loop through the queue families until we find a usable one (or not)
	int i = 0;
	// TODO: add logic to explicitly prefer a physical device that supports drawing and presentation in the same queue (better performance)
	for (const auto &queueFamily : queueFamilies) {
		// check for queue family graphics operations support
		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			indices.graphicsFamily = i;

		// check for queue family surface support
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
		if (queueFamily.queueCount > 0 && presentSupport)
			indices.presentFamily = i;

		if (indices.isComplete())
			break;
		i++;
	}
	return indices;
}

// ---------- SWAP CHAIN ---------- //

// checks that the device extensions specified at the top are available (includes swap chain extension)
bool Renderer::checkDeviceExtensionSupport(VkPhysicalDevice device) {
	// get available extensions list
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

	// confirm that all required extensions are in the extensions list
	std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());
	for (const auto &extension : availableExtensions) {
		requiredExtensions.erase(extension.extensionName);
	}
	return requiredExtensions.empty();
}

// get swap chain support details
SwapChainSupportDetails Renderer::querySwapChainSupport(VkPhysicalDevice physicalDevice) {
	SwapChainSupportDetails details;

	// surface capabilities (min/max number of images in swap chain, min/max width and height of images)
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &details.capabilities);

	// available surface formats (pixel format, color space)
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr);
	if (formatCount != 0) {
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, details.formats.data());
	}

	// available presentation modes (conditions for "swapping" images to the screen)
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, nullptr);
	if (presentModeCount != 0) {
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, details.presentModes.data());
	}

	return details;
}

// 1) choose a surface format
VkSurfaceFormatKHR Renderer::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &availableFormats) {
	// aRGB 8bit format (most common); SRBG color space (non-linear is better than linear rgb space);

	// the surface has no prefered format (best case scenario)
	if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
		return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	// see if prefered combination is available
	for (const auto &availableFormat : availableFormats) {
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			return availableFormat;
	}

	// otherwise just settle with the first format specified
	return availableFormats[0];
}

// 2) choose a presentation mode
VkPresentModeKHR Renderer::chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) {
	VkPresentModeKHR bestMode = VK_PRESENT_MODE_IMMEDIATE_KHR; // VK_PRESENT_MODE_FIFO_KHR
	for (const auto &availablePresentMode : availablePresentModes) {
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) // lets aim for triple buffering (see VkPresentModeKHR)
			return availablePresentMode;
	}
	return bestMode;
}

// 3) define swap extent
VkExtent2D Renderer::chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities) {
	// the swap extent is the resolution of the swap chain images
	if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
		return capabilities.currentExtent; // currentExtant = current window resolution
	} else {
		// set swap extent to window dimensions
		int width, height;
		pbInterface->GetFramebufferSize(&width, &height);
		VkExtent2D actualExtent = {
			static_cast<uint32_t>(width),
			static_cast<uint32_t>(height)
		};
		// but limited by the range of possible resolutions
		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));
		return actualExtent;
	}
}

// create a swap chain using the above configuration funcions
void Renderer::createSwapChain() {
	// get swap chain support details
	SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice);

	// set properties
	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
	VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
	VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

	// image count
	uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
	if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount)
		imageCount = swapChainSupport.capabilities.maxImageCount;

	// fill in swap chain info
	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = surface;
	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1; // used for stereographic 3D
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT; // render direct to image (no post processing)

	QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
	uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };
	// in case graphics queue family is different from the presentation queue
	if (indices.graphicsFamily != indices.presentFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	} else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE; // better performance
		createInfo.queueFamilyIndexCount = 0; // Optional
		createInfo.pQueueFamilyIndices = nullptr; // Optional
	}

	createInfo.preTransform = swapChainSupport.capabilities.currentTransform; // no transforms
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; // ignore alpha channel
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE; // don't care if pixels are obscured
	createInfo.oldSwapchain = VK_NULL_HANDLE; // e.g. new swap chain needs to be created if the window is resized

	// create a swap chain!
	if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create swap chain!");

	// store image handles
	vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
	swapChainImages.resize(imageCount);
	PB_INFO("swap chain image count = {}", imageCount);
	vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());

	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent = extent;
}

// create image views
void Renderer::createImageViews() {
	swapChainImageViews.resize(swapChainImages.size());

	for (uint32_t i = 0; i < swapChainImages.size(); i++) {
		swapChainImageViews[i] = createImageView(swapChainImages[i], swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1);
	}
}

// in case the surface changes (e.g. window resize)
void Renderer::recreateSwapChain() {
	PB_WARN("recreating swap chain");

	// pause when minimized
	pbInterface->MinimizeCheck();
	vkDeviceWaitIdle(device);

	cleanupSwapChain();

	createSwapChain();
	createImageViews();
	createRenderPass();
	createGraphicsPipelines();
	createDepthResources();
	createFramebuffers();
	createCommandBuffers();

	offscreen.UBOdata.proj = glm::perspective(glm::radians(45.0f), swapChainExtent.width / (float)swapChainExtent.height, 0.1f, 10.0f);
	offscreen.UBOdata.proj[1][1] *= -1;
}

// ---------- LOGICAL DEVICE AND QUEUES ---------- //

// enable GPU features
void Renderer::createLogicalDevice() {
	// get a suitable queue family (graphics capability)
	QueueFamilyIndices indices = findQueueFamilies(physicalDevice);

	// queue creation config for each queue family
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<uint32_t> uniqueQueueFamilies = {
		indices.graphicsFamily.value(),
		indices.presentFamily.value() }; // set removes duplicates

	float queuePriority = 1.0f;
	for (uint32_t queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	// device features
	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.samplerAnisotropy = VK_TRUE; // anisotropic filtering in texture sampler

	// device creation config
	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.pEnabledFeatures = &deviceFeatures;
	// device extensions
	createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();
	// validation layers (again)
	if (enableValidationLayers) {
		createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		createInfo.ppEnabledLayerNames = validationLayers.data();
	} else {
		createInfo.enabledLayerCount = 0;
	}

	// instantiate a logical device!
	if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS) {
		throw std::runtime_error("~ failed to create logical device!");
	}

	// set queue interface references
	vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &graphicsQueue);
	vkGetDeviceQueue(device, indices.presentFamily.value(), 0, &presentQueue);
}

// ---------- GRAPHICS PIPELINE SETUP ---------- //

void Renderer::createGraphicsPipelines() {

	// pipeline cache

	VkPipelineCacheCreateInfo cacheInfo{};
	cacheInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
	VkPipelineCache pipelineCache;
	vkCreatePipelineCache(device, &cacheInfo, nullptr, &pipelineCache);

	// OFFSCREEN PIPELINE

	auto vertShaderCode = readFile(OFFSCREEN_VERT_PATH);
	auto fragShaderCode = readFile(OFFSCREEN_FRAG_PATH);
	VkShaderModule vertShaderModule = createShaderModule(vertShaderCode);
	VkShaderModule fragShaderModule = createShaderModule(fragShaderCode);

	VkPipelineShaderStageCreateInfo vertShaderInfo = {};
	vertShaderInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderInfo.module = vertShaderModule;
	vertShaderInfo.pName = "main";

	VkPipelineShaderStageCreateInfo fragShaderInfo = {};
	fragShaderInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderInfo.module = fragShaderModule;
	fragShaderInfo.pName = "main";

	std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages = { vertShaderInfo, fragShaderInfo };

	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	std::vector<VkVertexInputBindingDescription> bindingDescriptions = Vertex::getBindingDescriptions();
	std::vector<VkVertexInputAttributeDescription> attributeDescriptions = Vertex::getAttributeDescriptions();
	vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescriptions.size());
	vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
	vertexInputInfo.pVertexBindingDescriptions = bindingDescriptions.data();
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	// viewport (transformation from swap chain image to framebuffer)
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)swapChainExtent.width;
	viewport.height = (float)swapChainExtent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	// scissor (pixel storage region)
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = swapChainExtent;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE; // useful for shadow maps
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL; // line/point modes require enabling a GPU feature
	rasterizer.lineWidth = 1.0f; // number of fragments
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE; // defines a front facing poly as having cw vertices
	rasterizer.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampling.minSampleShading = 1.0f;
	multisampling.alphaToCoverageEnable = VK_FALSE;
	multisampling.alphaToOneEnable = VK_FALSE;

	VkPipelineDepthStencilStateCreateInfo depthStencil = {};
	depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencil.depthTestEnable = VK_TRUE; // the depth test compares new fragments to the depth buffer to see if they should be discarded
	depthStencil.depthWriteEnable = VK_TRUE; // useful for drawing transparent objects
	depthStencil.depthCompareOp = VK_COMPARE_OP_LESS; // lower depth = closer, so new fragments should be less
	depthStencil.depthBoundsTestEnable = VK_FALSE; // optional depth bound test
	depthStencil.minDepthBounds = 0.0f;
	depthStencil.maxDepthBounds = 1.0f;

	std::array<VkPipelineColorBlendAttachmentState, 3> colorBlendAttachments{};
	colorBlendAttachments[offscreen.ALBEDO_LOCATION].colorWriteMask = 0xF;
	colorBlendAttachments[offscreen.ALBEDO_LOCATION].blendEnable = VK_FALSE;
	colorBlendAttachments[offscreen.POSITION_LOCATION].colorWriteMask = 0xF;
	colorBlendAttachments[offscreen.POSITION_LOCATION].blendEnable = VK_FALSE;
	colorBlendAttachments[offscreen.NORMAL_LOCATION].colorWriteMask = 0xF;
	colorBlendAttachments[offscreen.NORMAL_LOCATION].blendEnable = VK_FALSE;

	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = colorBlendAttachments.size();
	colorBlending.pAttachments = colorBlendAttachments.data();
	colorBlending.blendConstants[0] = 0.0f;
	colorBlending.blendConstants[1] = 0.0f;
	colorBlending.blendConstants[2] = 0.0f;
	colorBlending.blendConstants[3] = 0.0f;

	// dynamic states (can be changed without recreating the pipeline)

	// these are global variables to pass info to the shaders at draw time (description of binding locations)
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &offscreen.descriptorSetLayout; // which descriptors the shaders will be using

	if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &offscreen.pipelineLayout) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create pipeline layout!");

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = shaderStages.size();
	pipelineInfo.pStages = shaderStages.data();
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pDepthStencilState = &depthStencil;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDynamicState = nullptr;
	pipelineInfo.layout = offscreen.pipelineLayout;
	pipelineInfo.renderPass = renderPass;
	pipelineInfo.subpass = 0;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineInfo.basePipelineIndex = -1;

	if (vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineInfo, nullptr, &offscreen.pipeline) != VK_SUCCESS)
		throw std::runtime_error("failed to create graphics pipeline!");

	vkDestroyShaderModule(device, vertShaderModule, nullptr);
	vkDestroyShaderModule(device, fragShaderModule, nullptr);

	// COMPOSITION PIPELINE

	vertShaderCode = readFile(COMPOSITION_VERT_PATH);
	fragShaderCode = readFile(COMPOSITION_FRAG_PATH);
	vertShaderModule = createShaderModule(vertShaderCode);
	fragShaderModule = createShaderModule(fragShaderCode);
	vertShaderInfo.module = vertShaderModule;
	fragShaderInfo.module = fragShaderModule;

	uint32_t specializationData = NUM_LIGHTS;
	VkSpecializationMapEntry specializationEntry{};
	specializationEntry.constantID = 0;
	specializationEntry.offset = 0;
	specializationEntry.size = sizeof(specializationData);

	VkSpecializationInfo specializationInfo{};
	specializationInfo.mapEntryCount = 1;
	specializationInfo.pMapEntries = &specializationEntry;
	specializationInfo.dataSize = sizeof(specializationData);
	specializationInfo.pData = &specializationData;
	fragShaderInfo.pSpecializationInfo = &specializationInfo;

	shaderStages[0] = vertShaderInfo;
	shaderStages[1] = fragShaderInfo;

	VkPipelineVertexInputStateCreateInfo emptyInputState = {};
	emptyInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

	rasterizer.cullMode = VK_CULL_MODE_NONE;
	depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;

	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = 0xF;
	colorBlendAttachment.blendEnable = VK_FALSE;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;

	pipelineLayoutInfo.pSetLayouts = &composition.descriptorSetLayout;
	if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &composition.pipelineLayout) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create pipeline layout!");

	pipelineInfo.pVertexInputState = &emptyInputState;
	pipelineInfo.layout = composition.pipelineLayout;
	pipelineInfo.subpass = 1;
	if (vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineInfo, nullptr, &composition.pipeline) != VK_SUCCESS)
		throw std::runtime_error("failed to create graphics pipeline!");

	vkDestroyShaderModule(device, vertShaderModule, nullptr);
	vkDestroyShaderModule(device, fragShaderModule, nullptr);

	vkDestroyPipelineCache(device, pipelineCache, nullptr);
}

// binary data loader
std::vector<char> Renderer::readFile(const std::string& filename) {
	// open file
	std::ifstream file(filename, std::ios::ate | std::ios::binary); // ate: read from the end of the file; binary: file type
	if (!file.is_open())
		throw std::runtime_error("~ failed to open file!");

	// allocate a buffer
	size_t fileSize = (size_t)file.tellg(); // read position = file size
	std::vector<char> buffer(fileSize);

	// go back to beginning and read all bytes at once
	file.seekg(0);
	file.read(buffer.data(), fileSize);

	// close and return bytes
	file.close();
	return buffer;
}

// wrap SPIR-V code in a shader object
VkShaderModule Renderer::createShaderModule(const std::vector<char>& code) {
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	// assign shader code
	createInfo.codeSize = code.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data()); // convert char pointer to uint32_t pointer

	// create shader module
	VkShaderModule shaderModule;
	if (vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create shader module!");
	return shaderModule;
}

// ---------- RENDERPASS ---------- //

void Renderer::createRenderPass() {
	// deferred attachments		
	createAttachment(albedoAttachment, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);
	createAttachment(positionAttachment, VK_FORMAT_R16G16B16A16_SFLOAT, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);
	createAttachment(normalAttachment, VK_FORMAT_R16G16B16A16_SNORM, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);
	// TODO create depth buffer here
	std::array<VkAttachmentDescription, static_cast<int>(AttachmentIndices::COUNT)> attachments = {};

	// composition
	int index = static_cast<int>(AttachmentIndices::COMPOSITION);
	attachments[index].format = swapChainImageFormat;
	attachments[index].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[index].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; // black out the frame buffer before drawing a new frame
	attachments[index].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE; // because this is an intermediate for the colorResolve
	attachments[index].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE; // no stencil buffer
	attachments[index].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[index].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; // previous image doesn't need to be preserved
	attachments[index].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR; // not getting sampled for msaa

	// albedo
	index = static_cast<int>(AttachmentIndices::ALBEDO);
	attachments[index].format = this->albedoAttachment.format;
	attachments[index].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[index].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[index].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[index].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[index].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[index].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[index].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// position
	index = static_cast<int>(AttachmentIndices::POSITION);
	attachments[index].format = this->positionAttachment.format;
	attachments[index].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[index].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[index].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[index].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[index].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[index].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[index].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// normal
	index = static_cast<int>(AttachmentIndices::NORMAL);
	attachments[index].format = this->normalAttachment.format;
	attachments[index].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[index].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[index].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[index].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[index].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[index].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[index].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// depth attachment
	index = static_cast<int>(AttachmentIndices::DEPTH);
	attachments[index].format = findDepthFormat(); // same as the depth image
	attachments[index].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[index].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[index].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE; // won't be used after the draw is finished
	attachments[index].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[index].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[index].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; // don't care about previous depth contents
	attachments[index].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	std::array<VkSubpassDescription, 2> subpasses{};

	// 0) first subpass - offscreen gbuffer components
	{
		std::array<VkAttachmentReference, 3> colorAttachmentRefs{};
		colorAttachmentRefs[offscreen.ALBEDO_LOCATION].attachment = static_cast<int>(AttachmentIndices::ALBEDO);
		colorAttachmentRefs[offscreen.ALBEDO_LOCATION].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		colorAttachmentRefs[offscreen.POSITION_LOCATION].attachment = static_cast<int>(AttachmentIndices::POSITION);
		colorAttachmentRefs[offscreen.POSITION_LOCATION].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		colorAttachmentRefs[offscreen.NORMAL_LOCATION].attachment = static_cast<int>(AttachmentIndices::NORMAL);
		colorAttachmentRefs[offscreen.NORMAL_LOCATION].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkAttachmentReference depthAttachmentRef{};
		depthAttachmentRef.attachment = static_cast<int>(AttachmentIndices::DEPTH);
		depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		subpasses[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpasses[0].colorAttachmentCount = colorAttachmentRefs.size();
		subpasses[0].pColorAttachments = colorAttachmentRefs.data();
		subpasses[0].pDepthStencilAttachment = &depthAttachmentRef;
	}

	// 1) second subpass - final composition
	{
		VkAttachmentReference colorAttachmentRef{};
		colorAttachmentRef.attachment = static_cast<int>(AttachmentIndices::COMPOSITION);
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		std::array<VkAttachmentReference, 3> inputAttachmentRefs{};
		inputAttachmentRefs[composition.ALBEDO_BINDING].attachment = static_cast<int>(AttachmentIndices::ALBEDO);
		inputAttachmentRefs[composition.ALBEDO_BINDING].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		inputAttachmentRefs[composition.POSITION_BINDING].attachment = static_cast<int>(AttachmentIndices::POSITION);
		inputAttachmentRefs[composition.POSITION_BINDING].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		inputAttachmentRefs[composition.NORMAL_BINDING].attachment = static_cast<int>(AttachmentIndices::NORMAL);
		inputAttachmentRefs[composition.NORMAL_BINDING].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		subpasses[1].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpasses[1].inputAttachmentCount = inputAttachmentRefs.size();
		subpasses[1].pInputAttachments = inputAttachmentRefs.data();
		subpasses[1].colorAttachmentCount = 1;
		subpasses[1].pColorAttachments = &colorAttachmentRef;
	}

	std::array<VkSubpassDependency, 3> subpassDependencies{};

	// start of renderpass
	subpassDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	subpassDependencies[0].dstSubpass = 0;
	subpassDependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	subpassDependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	subpassDependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	subpassDependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// transform input attachments from color attachment to shader read
	subpassDependencies[1].srcSubpass = 0;
	subpassDependencies[1].dstSubpass = 1;
	subpassDependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependencies[1].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	subpassDependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	subpassDependencies[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
	subpassDependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// TODO needed?
	subpassDependencies[2].srcSubpass = 0;
	subpassDependencies[2].dstSubpass = VK_SUBPASS_EXTERNAL;
	subpassDependencies[2].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependencies[2].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	subpassDependencies[2].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	subpassDependencies[2].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	subpassDependencies[2].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// render pass
	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderPassInfo.pAttachments = attachments.data(); // attachments array
	renderPassInfo.subpassCount = static_cast<uint32_t>(subpasses.size());
	renderPassInfo.pSubpasses = subpasses.data(); // subpasses array
	renderPassInfo.dependencyCount = static_cast<uint32_t>(subpassDependencies.size());
	renderPassInfo.pDependencies = subpassDependencies.data();

	if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
		throw std::runtime_error("failed to create render pass!");
}

// TODO use this to create depth attachment
void Renderer::createAttachment(Attachment& attachment, VkFormat format, VkImageUsageFlags usage) {
	VkImageAspectFlags aspectMask = 0;
	attachment.format = format;

	if (usage & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT) {
		aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}
	if (usage & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) {
		aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
	}

	assert(aspectMask > 0);

	VkImageCreateInfo image{};
	image.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	image.imageType = VK_IMAGE_TYPE_2D;
	image.format = format;
	image.extent.width = swapChainExtent.width;
	image.extent.height = swapChainExtent.height;
	image.extent.depth = 1;
	image.mipLevels = 1;
	image.arrayLayers = 1;
	image.samples = VK_SAMPLE_COUNT_1_BIT;
	image.tiling = VK_IMAGE_TILING_OPTIMAL;
	image.usage = usage | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;	// VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT flag is required for input attachments;
	image.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	VkMemoryAllocateInfo memAlloc{};
	memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	VkMemoryRequirements memReqs;

	if (vkCreateImage(device, &image, nullptr, &attachment.image) != VK_SUCCESS)
		throw std::runtime_error("failed attachment image creation!");
	vkGetImageMemoryRequirements(device, attachment.image, &memReqs);
	memAlloc.allocationSize = memReqs.size;
	memAlloc.memoryTypeIndex = findMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	if (vkAllocateMemory(device, &memAlloc, nullptr, &attachment.memory) != VK_SUCCESS)
		throw std::runtime_error("failed attachment memory allocation");
	if (vkBindImageMemory(device, attachment.image, attachment.memory, 0) != VK_SUCCESS)
		throw std::runtime_error("failed attachment image bind");

	// imageView
	VkImageViewCreateInfo imageView = {};
	imageView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	imageView.viewType = VK_IMAGE_VIEW_TYPE_2D;
	imageView.format = format;
	imageView.subresourceRange = {};
	imageView.subresourceRange.aspectMask = aspectMask;
	imageView.subresourceRange.baseMipLevel = 0;
	imageView.subresourceRange.levelCount = 1;
	imageView.subresourceRange.baseArrayLayer = 0;
	imageView.subresourceRange.layerCount = 1;
	imageView.image = attachment.image;
	if (vkCreateImageView(device, &imageView, nullptr, &attachment.imageView) != VK_SUCCESS)
		throw std::runtime_error("failed create attachment image view");
}

void Renderer::createFramebuffers() {
	// create framebuffers for each image view
	swapChainFramebuffers.resize(swapChainImageViews.size());

	std::array<VkImageView, static_cast<int>(AttachmentIndices::COUNT)> attachments{};
	// attachments[AttachmentIndices::COMPOSITION] = swap chain image
	attachments[static_cast<int>(AttachmentIndices::ALBEDO)] = albedoAttachment.imageView;
	attachments[static_cast<int>(AttachmentIndices::POSITION)] = positionAttachment.imageView;
	attachments[static_cast<int>(AttachmentIndices::NORMAL)] = normalAttachment.imageView;
	attachments[static_cast<int>(AttachmentIndices::DEPTH)] = depthAttachment.imageView;

	VkFramebufferCreateInfo framebufferInfo = {};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferInfo.renderPass = renderPass;
	framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	framebufferInfo.pAttachments = attachments.data();
	framebufferInfo.width = swapChainExtent.width;
	framebufferInfo.height = swapChainExtent.height;
	framebufferInfo.layers = 1;

	for (size_t i = 0; i < swapChainImageViews.size(); i++) {
		attachments[static_cast<int>(AttachmentIndices::COMPOSITION)] = swapChainImageViews[i];

		if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i]) != VK_SUCCESS)
			throw std::runtime_error("~ failed to create framebuffer!");
	}
}

// ---------- VERTEX AND INDEX BUFFERS ---------- //

// load vertices and indices from an external model
void Renderer::loadModels() {
	PB_INFO("starting model loading");
	for (int m = 0; m < NUM_MODELS; m++) {
		// load vertices and indices
		models[m].Init(m);

		// set instance data
		models[m].instances.resize(pbModels::modelConfigs[m].instancePositions.size());

		for (int i = 0; i < models[m].instances.size(); i++) {
			glm::mat4 modelMatrix;
			modelMatrix = glm::rotate(glm::mat4(1.0f), 0.0f, glm::vec3(0.0f, 0.0f, 1.0f));
			modelMatrix = glm::translate(modelMatrix, pbModels::modelConfigs[m].instancePositions[i]);
			models[m].instances[i] = { modelMatrix[0], modelMatrix[1], modelMatrix[2], modelMatrix[3] };
		}
	}
	PB_INFO("finished model loading");
}

void Renderer::createGraphicsBuffers() {
	vertices.clear();
	indices.clear();
	instances.clear();

	for (int m = 0; m < NUM_MODELS; m++) {
		models[m].vertexOffset = vertices.size();
		models[m].indexOffset = indices.size();
		models[m].instanceOffset = instances.size();

		vertices.insert(vertices.end(), models[m].vertices.begin(), models[m].vertices.end());
		indices.insert(indices.end(), models[m].indices.begin(), models[m].indices.end());
		instances.insert(instances.end(), models[m].instances.begin(), models[m].instances.end());

		models[m].vertices.clear();
		models[m].indices.clear();
		models[m].instances.clear();
	}

	// vertex buffer
	createGraphicsBuffer(sizeof(Vertex) *vertices.size(), vertices.data(), vertexBuffer, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
	// index buffer
	createGraphicsBuffer(sizeof(uint32_t) *indices.size(), indices.data(), indexBuffer, VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
	// instance buffer
	createGraphicsBuffer(sizeof(InstanceData) *instances.size(), instances.data(), instanceBuffer, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
}

void Renderer::createGraphicsBuffer(VkDeviceSize bufferSize, const void* src, Buffer &buffer, VkBufferUsageFlagBits bufferUsage) {
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, src, (size_t)bufferSize);
	vkUnmapMemory(device, stagingBufferMemory);

	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | bufferUsage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer.buffer, buffer.memory);

	copyBuffer(stagingBuffer, buffer.buffer, bufferSize);

	vkDestroyBuffer(device, stagingBuffer, nullptr);
	vkFreeMemory(device, stagingBufferMemory, nullptr);
}

// creates a buffer and binds memory to it
void Renderer::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory) {
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create buffer!");

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

	if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) // TODO: memory allocation https://vulkan-tutorial.com/Vertex_buffers/Staging_buffer#page_Conclusion
		throw std::runtime_error("~ failed to allocate buffer memory!");

	vkBindBufferMemory(device, buffer, bufferMemory, 0);
}

// used to transfer data from the staging buffer to the vertex buffer (as part of CPU to GPU data transfer)
void Renderer::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
	VkCommandBuffer commandBuffer = beginSingleTimeCommands();

	// use vkCmdCopyBuffer to transfer buffer content
	VkBufferCopy copyRegion = {};
	copyRegion.srcOffset = 0; // Optional
	copyRegion.dstOffset = 0; // Optional
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

	endSingleTimeCommands(commandBuffer);
}

VkCommandBuffer Renderer::beginSingleTimeCommands() {
	// create temporary command buffer
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = commandPool; // TODO: have seperate command pool for memory alocation optimisations
	allocInfo.commandBufferCount = 1;
	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

	// start command recording
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	return commandBuffer;
}

void Renderer::endSingleTimeCommands(VkCommandBuffer commandBuffer) {
	// stop recording
	vkEndCommandBuffer(commandBuffer);

	// execute the commmand buffer
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	// TODO: create seperate transfer queue https://vulkan-tutorial.com/Vertex_buffers/Staging_buffer#page_Transfer_queue useful for texture streaming
	vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE); // both the graphics and compute queues implicity support VK_QUEUE_TRANSFER_BIT functionality
	vkQueueWaitIdle(graphicsQueue); // wait until done (use a fence to scedule multiple transfers at once

	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer); // clean up the command buffer
}

// find the type of memory to allocate
uint32_t Renderer::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
			return i;
	}
	throw std::runtime_error("~ failed to find suitable memory type!");
}

// ---------- COMMAND BUFFERS ---------- //

void Renderer::createCommandPool() {
	QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice);

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value(); // for drawing commands
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	if (vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create command pool!");
}

void Renderer::createCommandBuffers() {
	commandBuffers.resize(swapChainFramebuffers.size());

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

	if (vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()) != VK_SUCCESS)
		throw std::runtime_error("failed to allocate command buffers!");

	// for buffer binding
	VkDeviceSize offsets[] = { 0, 0 };
	VkBuffer buffers[] = { vertexBuffer.buffer, instanceBuffer.buffer };

	// attachments and depth buffer
	std::array<VkClearValue, static_cast<int>(AttachmentIndices::COUNT)> clearValues{};
	clearValues[static_cast<int>(AttachmentIndices::COMPOSITION)].color = { 0.0f, 0.0f, 0.0f, 1.0f };
	clearValues[static_cast<int>(AttachmentIndices::ALBEDO)].color = { 0.0f, 0.0f, 0.0f, 1.0f };
	clearValues[static_cast<int>(AttachmentIndices::POSITION)].color = { 0.0f, 0.0f, 0.0f, 1.0f };
	clearValues[static_cast<int>(AttachmentIndices::NORMAL)].color = { 0.0f, 0.0f, 0.0f, 1.0f };
	clearValues[static_cast<int>(AttachmentIndices::DEPTH)].depthStencil = { 1.0f, 0 };

	for (size_t i = 0; i < commandBuffers.size(); i++) {
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = renderPass;
		renderPassInfo.framebuffer = swapChainFramebuffers[i];
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapChainExtent;

		renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		renderPassInfo.pClearValues = clearValues.data();

		if (vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS)
			throw std::runtime_error("failed to begin recording command buffer!");

		vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

		///////////////////////////

			// subpass 0 - render the scene and write g-buffer attachments
			vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, offscreen.pipeline);

			vkCmdBindVertexBuffers(commandBuffers[i], 0, 2, buffers, offsets);
			vkCmdBindIndexBuffer(commandBuffers[i], indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);

			// render our models
			for (int m = 0; m < NUM_MODELS; m++) {

				// descriptor set contains model and texture data (shader bindings)
				vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, offscreen.pipelineLayout, 0, 1,
					&offscreen.descriptorSets[m], 0, nullptr); // TODO bind all sets at once and use dynamic offsets

				// our drawing command!
				vkCmdDrawIndexed(commandBuffers[i], models[m].indexCount, models[m].instanceCount,
					models[m].indexOffset, models[m].vertexOffset, models[m].instanceOffset);
			}

			// subpass 1 - use g-buffer components to compose the final image
			vkCmdNextSubpass(commandBuffers[i], VK_SUBPASS_CONTENTS_INLINE);
			vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, composition.pipeline);

			vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, composition.pipelineLayout, 0, 1,
				&composition.descriptorSet, 0, nullptr);
			vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

		///////////////////////////

		vkCmdEndRenderPass(commandBuffers[i]);

		if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS)
			throw std::runtime_error("failed to record command buffer!");
	}
}

// ---------- UNIFORM BUFFERS ---------- //

// need multiple buffers because there are multiple command buffers (corresponding to the swap chain images)
void Renderer::createUniformBuffers() {
	createBuffer(sizeof(offscreen.UBOdata), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		offscreen.UBO.buffer, offscreen.UBO.memory);
	createBuffer(sizeof(pbModels::lightSources), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		composition.UBO.buffer, composition.UBO.memory);
}

void Renderer::InitUniformBuffer(glm::mat4 view) {
	offscreen.UBOdata.view = view;
	offscreen.UBOdata.proj = glm::perspective(glm::radians(45.0f), swapChainExtent.width / (float)swapChainExtent.height, 0.1f, 1000.0f);
	offscreen.UBOdata.proj[1][1] *= -1; // GLM designed for opengl -> have to flip y axis

	// glm::mat4 glm::rotate(glm::mat4 const& m, float angle, glm::vec3 const& axis); m = input matrix, angle = radians, axis = ideally normalized
	// glm::mat4 glm::lookAt(glm::vec3 const& eye, glm::vec3 const& center, glm::vec3const& up); eye = position of camera, center = where the camera is looking at, up = normalized up vector
	// glm::mat4 perspective(float fovy, float aspect, float zNear, float zFar); fovy = fov angle in v.y direction, aspect = fov ratio v.x/v.y, zNear = distance from viewer to near clipping plane, zFar = far clipping plane

	updateUniformBuffers();
}

void Renderer::updateUniformBuffers() {
	void* data;

	vkMapMemory(device, offscreen.UBO.memory, 0, sizeof(offscreen.UBOdata), 0, &data);
	memcpy(data, &offscreen.UBOdata, sizeof(offscreen.UBOdata));
	vkUnmapMemory(device, offscreen.UBO.memory);

	vkMapMemory(device, composition.UBO.memory, 0, sizeof(pbModels::lightSources), 0, &data);
	memcpy(data, pbModels::lightSources.data(), sizeof(pbModels::lightSources));
	vkUnmapMemory(device, composition.UBO.memory);
}

// ---------- DESCRIPTOR CREATION ---------- //

// a descriptor is a way for shaders to freely access resources like buffers (e.g. perspective transforms) and images (e.g. textures)
// the layout describes the types of descriptors that can be bound for pipeline creation
void Renderer::createDescriptorSetLayout() {

	// OFFSCREEN

	std::array<VkDescriptorSetLayoutBinding, 3> offscreenBindings{};

	// UBO
	offscreenBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	offscreenBindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	offscreenBindings[0].binding = offscreen.UBO_BINDING;
	offscreenBindings[0].descriptorCount = 1;

	// textures
	offscreenBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
	offscreenBindings[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	offscreenBindings[1].binding = offscreen.TEXTURE_BINDING;
	offscreenBindings[1].descriptorCount = 1;
	
	// samplers
	offscreenBindings[2].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
	offscreenBindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	offscreenBindings[2].binding = offscreen.SAMPLER_BINDING;
	offscreenBindings[2].descriptorCount = 1;

	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(offscreenBindings.size());
	layoutInfo.pBindings = offscreenBindings.data();

	if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &offscreen.descriptorSetLayout) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create descriptor set layout!");

	// COMPOSITION

	std::array<VkDescriptorSetLayoutBinding, 4> compositionBindings{};

	// albedo
	compositionBindings[composition.ALBEDO_BINDING].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
	compositionBindings[composition.ALBEDO_BINDING].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	compositionBindings[composition.ALBEDO_BINDING].binding = composition.ALBEDO_BINDING;
	compositionBindings[composition.ALBEDO_BINDING].descriptorCount = 1;

	// positon
	compositionBindings[composition.POSITION_BINDING].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
	compositionBindings[composition.POSITION_BINDING].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	compositionBindings[composition.POSITION_BINDING].binding = composition.POSITION_BINDING;
	compositionBindings[composition.POSITION_BINDING].descriptorCount = 1;

	// normal
	compositionBindings[composition.NORMAL_BINDING].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
	compositionBindings[composition.NORMAL_BINDING].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	compositionBindings[composition.NORMAL_BINDING].binding = composition.NORMAL_BINDING;
	compositionBindings[composition.NORMAL_BINDING].descriptorCount = 1;

	// UBO
	compositionBindings[composition.UBO_BINDING].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	compositionBindings[composition.UBO_BINDING].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	compositionBindings[composition.UBO_BINDING].binding = composition.UBO_BINDING;
	compositionBindings[composition.UBO_BINDING].descriptorCount = 1;

	layoutInfo.bindingCount = static_cast<uint32_t>(compositionBindings.size());
	layoutInfo.pBindings = compositionBindings.data();

	if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &composition.descriptorSetLayout) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create descriptor set layout!");
}

// the pool is used to allocate descriptor sets (of some layout) for use in a shader
void Renderer::createDescriptorPool() {
	std::array<VkDescriptorPoolSize, 4> poolSizes = {};

	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = NUM_MODELS + 1;

	poolSizes[1].type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
	poolSizes[1].descriptorCount = NUM_MODELS;

	poolSizes[2].type = VK_DESCRIPTOR_TYPE_SAMPLER;
	poolSizes[2].descriptorCount = NUM_MODELS; // TODO can the models share a sampler?

	poolSizes[3].type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
	poolSizes[3].descriptorCount = 3;

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = NUM_MODELS + 1;

	if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create descriptor pool!");
}

// a descriptor set	specifies the actual buffer or image resources that will be bound to the descriptors (like the frambuffer)
// here we bind the actual resources to the descriptor
void Renderer::createDescriptorSets() {

	// OFFSCREEN
	{
		std::array<VkDescriptorSetLayout, NUM_MODELS> layouts;
		layouts.fill(offscreen.descriptorSetLayout);
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = descriptorPool;
		allocInfo.descriptorSetCount = static_cast<uint32_t>(layouts.size());
		allocInfo.pSetLayouts = layouts.data();

		if (vkAllocateDescriptorSets(device, &allocInfo, offscreen.descriptorSets.data()) != VK_SUCCESS)
			throw std::runtime_error("failed to allocate descriptor sets!");

		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.buffer = offscreen.UBO.buffer;
		bufferInfo.offset = 0;
		bufferInfo.range = sizeof(offscreen.UBOdata);

		VkDescriptorImageInfo samplerInfo = {};
		samplerInfo.sampler = offscreen.sampler;

		for (int m = 0; m < offscreen.descriptorSets.size(); m++) {
			std::array<VkWriteDescriptorSet, 3> descriptorWrites = {};

			// ubo
			descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[0].dstSet = offscreen.descriptorSets[m];
			descriptorWrites[0].dstBinding = offscreen.UBO_BINDING;
			descriptorWrites[0].dstArrayElement = 0;
			descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrites[0].descriptorCount = 1;
			descriptorWrites[0].pBufferInfo = &bufferInfo;

			// texture
			VkDescriptorImageInfo imageInfo;
			imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageInfo.imageView = models[m].textureImageView;
			imageInfo.sampler = nullptr;

			descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[1].dstSet = offscreen.descriptorSets[m];
			descriptorWrites[1].dstBinding = offscreen.TEXTURE_BINDING;
			descriptorWrites[1].dstArrayElement = 0;
			descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
			descriptorWrites[1].descriptorCount = 1;
			descriptorWrites[1].pImageInfo = &imageInfo;

			// sampler
			descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[2].dstSet = offscreen.descriptorSets[m];
			descriptorWrites[2].dstBinding = offscreen.SAMPLER_BINDING;
			descriptorWrites[2].dstArrayElement = 0;
			descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
			descriptorWrites[2].descriptorCount = 1;
			descriptorWrites[2].pImageInfo = &samplerInfo;

			vkUpdateDescriptorSets(device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
		}
	}

	// COMPOSITION
	{
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = descriptorPool;
		allocInfo.descriptorSetCount = 1;
		allocInfo.pSetLayouts = &composition.descriptorSetLayout;

		if (vkAllocateDescriptorSets(device, &allocInfo, &composition.descriptorSet) != VK_SUCCESS)
			throw std::runtime_error("failed to allocate descriptor sets!");

		std::array<VkWriteDescriptorSet, 4> descriptorWrites{};

		// albedo
		VkDescriptorImageInfo albedoInfo;
		albedoInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		albedoInfo.imageView = albedoAttachment.imageView;
		albedoInfo.sampler = VK_NULL_HANDLE;

		descriptorWrites[composition.ALBEDO_BINDING].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[composition.ALBEDO_BINDING].dstSet = composition.descriptorSet;
		descriptorWrites[composition.ALBEDO_BINDING].dstBinding = composition.ALBEDO_BINDING;
		descriptorWrites[composition.ALBEDO_BINDING].dstArrayElement = 0;
		descriptorWrites[composition.ALBEDO_BINDING].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		descriptorWrites[composition.ALBEDO_BINDING].descriptorCount = 1;
		descriptorWrites[composition.ALBEDO_BINDING].pImageInfo = &albedoInfo;

		// position
		VkDescriptorImageInfo positionInfo;
		positionInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		positionInfo.imageView = positionAttachment.imageView;
		positionInfo.sampler = VK_NULL_HANDLE;

		descriptorWrites[composition.POSITION_BINDING].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[composition.POSITION_BINDING].dstSet = composition.descriptorSet;
		descriptorWrites[composition.POSITION_BINDING].dstBinding = composition.POSITION_BINDING;
		descriptorWrites[composition.POSITION_BINDING].dstArrayElement = 0;
		descriptorWrites[composition.POSITION_BINDING].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		descriptorWrites[composition.POSITION_BINDING].descriptorCount = 1;
		descriptorWrites[composition.POSITION_BINDING].pImageInfo = &positionInfo;

		// normal
		VkDescriptorImageInfo normalInfo;
		normalInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		normalInfo.imageView = normalAttachment.imageView;
		normalInfo.sampler = VK_NULL_HANDLE;

		descriptorWrites[composition.NORMAL_BINDING].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[composition.NORMAL_BINDING].dstSet = composition.descriptorSet;
		descriptorWrites[composition.NORMAL_BINDING].dstBinding = composition.NORMAL_BINDING;
		descriptorWrites[composition.NORMAL_BINDING].dstArrayElement = 0;
		descriptorWrites[composition.NORMAL_BINDING].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		descriptorWrites[composition.NORMAL_BINDING].descriptorCount = 1;
		descriptorWrites[composition.NORMAL_BINDING].pImageInfo = &normalInfo;

		// ubo
		VkDescriptorBufferInfo uboInfo = {};
		uboInfo.buffer = composition.UBO.buffer;
		uboInfo.offset = 0;
		uboInfo.range = sizeof(pbModels::lightSources);

		descriptorWrites[composition.UBO_BINDING].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[composition.UBO_BINDING].dstSet = composition.descriptorSet;
		descriptorWrites[composition.UBO_BINDING].dstBinding = composition.UBO_BINDING;
		descriptorWrites[composition.UBO_BINDING].dstArrayElement = 0;
		descriptorWrites[composition.UBO_BINDING].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		descriptorWrites[composition.UBO_BINDING].descriptorCount = 1;
		descriptorWrites[composition.UBO_BINDING].pBufferInfo = &uboInfo;

		vkUpdateDescriptorSets(device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
	}
}

// ---------- SEMAPHORES AND FENCES ---------- //

// semaphores are used to synch up those cheeky command queue operations
// fences are used to sync up application code
void Renderer::createSyncObjects() {
	imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT; // allows the first frame to run

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS ||
			vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS ||
			vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS) {
			throw std::runtime_error("~ failed to create synchronization objects for a frame!");
		}
	}
}

// ---------- TEXTURE MAPPING ---------- //

void Renderer::createTextureImages() {
	for (int i = 0; i < NUM_MODELS; i++) {

		int texWidth, texHeight, texChannels;
		stbi_uc *pixels = stbi_load(models[i].GetTexturePath().c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
		VkDeviceSize imageSize = texWidth * texHeight * 4;
		models[i].mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(texWidth, texHeight)))) + 1;

		if (!pixels) {
			PB_ERROR("failed to load texture image for model: " + models[i].config->name);
			throw std::runtime_error("~ failed to load texture image!");
		}

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;

		createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer, stagingBufferMemory);

		// copy pixel values to buffer
		void* data;
		vkMapMemory(device, stagingBufferMemory, 0, imageSize, 0, &data);
		memcpy(data, pixels, static_cast<size_t>(imageSize));
		vkUnmapMemory(device, stagingBufferMemory);

		stbi_image_free(pixels); // clean original pixel array

		createImage(texWidth, texHeight, models[i].mipLevels, VK_SAMPLE_COUNT_1_BIT, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
			VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			models[i].textureImage, models[i].textureImageMemory);

		// transition the image to VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
		transitionImageLayout(models[i].textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, models[i].mipLevels);
		// execute the buffer to image copy operation
		copyBufferToImage(stagingBuffer, models[i].textureImage, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
		// prepare the texture image for shader access
		// transitioned to VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL while generating mipmaps
		generateMipmaps(models[i].textureImage, VK_FORMAT_R8G8B8A8_UNORM, texWidth, texHeight, models[i].mipLevels);

		// clean up staging buffer
		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}
}

void Renderer::createImage(uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples, VkFormat format, VkImageTiling tiling,
	VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory) {
	// create vulkan texture object
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = mipLevels; // mipmapping
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling; // can't directly access pixels
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; // not usable by the GPU and the very first transition will discard the texels
	imageInfo.usage = usage;
	imageInfo.samples = numSamples; // multisampling
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE; // only one queue family
	imageInfo.flags = 0; // if you want to use 3d texture for voxel terrain
	if (vkCreateImage(device, &imageInfo, nullptr, &image) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create image!");

	// allocate memory
	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(device, image, &memRequirements);
	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);
	if (vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS)
		throw std::runtime_error("~ failed to allocate image memory!");
	vkBindImageMemory(device, image, imageMemory, 0);
}

// transfer an image between layouts so we can record and execute vkCmdCopyBufferToImage
void Renderer::transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels) {
	VkCommandBuffer commandBuffer = beginSingleTimeCommands();

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED; // if we want to use the barrier to transfer queue family ownership
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
	if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		if (hasStencilComponent(format))
			barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
	} else {
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = mipLevels; // mipmapping
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	VkPipelineStageFlags sourceStage;
	VkPipelineStageFlags destinationStage;

	if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	} else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	} else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	} else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	} else {
		throw std::invalid_argument("unsupported layout transition!");
	}

	vkCmdPipelineBarrier(
		commandBuffer,
		sourceStage, destinationStage,
		0,
		0, nullptr,
		0, nullptr,
		1, &barrier
	);

	endSingleTimeCommands(commandBuffer);
}

void Renderer::copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
	VkCommandBuffer commandBuffer = beginSingleTimeCommands();

	VkBufferImageCopy region = {};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;

	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;

	region.imageOffset = { 0, 0, 0 };
	region.imageExtent = {
		width,
		height,
		1
	};

	// enqueue buffer to image copy operation
	vkCmdCopyBufferToImage(
		commandBuffer,
		buffer,
		image,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1,
		&region
	);

	endSingleTimeCommands(commandBuffer);
}

void Renderer::createTextureImageViews() {
	for (int i = 0; i < NUM_MODELS; i++) {
		models[i].textureImageView = createImageView(models[i].textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT, models[i].mipLevels);
	}
}

// used by both createTextureImageView and createImageViews
VkImageView Renderer::createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels) {
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	// set parameters
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = mipLevels; // mipmapping
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	VkImageView imageView;
	if (vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create texture image view!");

	return imageView;
}

// sampler is used to read colors from the texture in the shader - has all sorts of cool texture processes!
void Renderer::createTextureSampler() {
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR; // oversapling - interpolation method (TODO: use nearest to pixelate texture?)
	samplerInfo.minFilter = VK_FILTER_LINEAR; // undersampling

	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT; // u,v,w for texture space coordinates
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT; // how the shader handles space outside texture (repeating)
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

	samplerInfo.anisotropyEnable = VK_TRUE; // undersampling mitigation - anisotropic filtering
	samplerInfo.maxAnisotropy = 16;

	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK; // for clamp to border addressing mode

	samplerInfo.unnormalizedCoordinates = VK_FALSE; // coordinates are normalized range = [0, 1]

	samplerInfo.compareEnable = VK_FALSE; // can be used for percentage-closer filtering on shadow maps
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR; // used for mipmapping
	samplerInfo.minLod = 0; // Optional
	uint32_t mipLevels[NUM_MODELS];
	for (int i = 0; i < NUM_MODELS; i++) { mipLevels[i] = models[i].mipLevels; }
	samplerInfo.maxLod = static_cast<float>(*std::max_element(std::begin(mipLevels), std::end(mipLevels)));
	samplerInfo.mipLodBias = 0; // Optional

	if (vkCreateSampler(device, &samplerInfo, nullptr, &offscreen.sampler) != VK_SUCCESS)
		throw std::runtime_error("~ failed to create texture sampler!");
}

// generates mipmaps of a texture vkimage
void Renderer::generateMipmaps(VkImage image, VkFormat imageFormat, int32_t texWidth, int32_t texHeight, uint32_t mipLevels) {
	// Check if image format supports linear blitting
	VkFormatProperties formatProperties;
	vkGetPhysicalDeviceFormatProperties(physicalDevice, imageFormat, &formatProperties);
	if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT))
		throw std::runtime_error("~ texture image format does not support linear blitting!");

	VkCommandBuffer commandBuffer = beginSingleTimeCommands();

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.image = image;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;
	barrier.subresourceRange.levelCount = 1;

	int32_t mipWidth = texWidth;
	int32_t mipHeight = texHeight;

	for (uint32_t i = 1; i < mipLevels; i++) {
		// transition level i-1 to VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL and wait for level i - 1 to be filled, either by vkCmdCopyBufferToImage or the previous blit
		barrier.subresourceRange.baseMipLevel = i - 1;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		vkCmdPipelineBarrier(commandBuffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
			0, nullptr,
			0, nullptr,
			1, &barrier);

		// use the blit command to perform copying, scaling and filtering
		VkImageBlit blit = {};
		blit.srcOffsets[0] = { 0, 0, 0 }; // source
		blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
		blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.srcSubresource.mipLevel = i - 1;
		blit.srcSubresource.baseArrayLayer = 0;
		blit.srcSubresource.layerCount = 1;
		blit.dstOffsets[0] = { 0, 0, 0 }; // destination
		blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
		blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.dstSubresource.mipLevel = i;
		blit.dstSubresource.baseArrayLayer = 0;
		blit.dstSubresource.layerCount = 1;
		vkCmdBlitImage(commandBuffer,
			image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1, &blit,
			VK_FILTER_LINEAR); // enables interpolation

		// transition to VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL (waits for blit to finish)
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		vkCmdPipelineBarrier(commandBuffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
			0, nullptr,
			0, nullptr,
			1, &barrier);

		if (mipWidth > 1) mipWidth /= 2;
		if (mipHeight > 1) mipHeight /= 2;
	}

	barrier.subresourceRange.baseMipLevel = mipLevels - 1;
	barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
	vkCmdPipelineBarrier(commandBuffer,
		VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
		0, nullptr,
		0, nullptr,
		1, &barrier);

	endSingleTimeCommands(commandBuffer);
}

// ---------- DEPTH BUFFERING ---------- //

void Renderer::createDepthResources() {
	VkFormat depthFormat = findDepthFormat();
	createImage(swapChainExtent.width, swapChainExtent.height, 1, VK_SAMPLE_COUNT_1_BIT, depthFormat, VK_IMAGE_TILING_OPTIMAL,
		VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthAttachment.image, depthAttachment.memory);
	depthAttachment.imageView = createImageView(depthAttachment.image, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);
	transitionImageLayout(depthAttachment.image, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1);
}

bool Renderer::hasStencilComponent(VkFormat format) {
	return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkFormat Renderer::findDepthFormat() {
	return findSupportedFormat(
		{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
		VK_IMAGE_TILING_OPTIMAL,
		VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
	);
}

VkFormat Renderer::findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
	for (VkFormat format : candidates) {
		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);
		if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
			return format;
		} else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}
	throw std::runtime_error("~ failed to find supported format!");
}

// ---------- MULTISAMPLING ---------- //

// multisampling anti-aliasing
VkSampleCountFlagBits Renderer::getMinUsableSampleCount() {
	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);

	VkSampleCountFlags counts = std::min(physicalDeviceProperties.limits.framebufferColorSampleCounts, physicalDeviceProperties.limits.framebufferDepthSampleCounts);
	if (counts & VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }
	if (counts & VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
	if (counts & VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
	if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
	if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
	if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
	PB_WARN("MSAA not supported");
	return VK_SAMPLE_COUNT_1_BIT;
}

// ---------- RENDERING AND PRESENTATION LOOP ---------- //

void Renderer::DrawFrame(bool windowResized) {
	// wait for the frame to finish
	vkWaitForFences(device, 1, &inFlightFences[currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max());

	// 1) acquire an image from the swap chain
	uint32_t imageIndex; // which VkImage in our swapChainImages array has become available
	VkResult result = vkAcquireNextImageKHR(device, swapChain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphores[currentFrame],
		VK_NULL_HANDLE, &imageIndex);
	if (result == VK_ERROR_OUT_OF_DATE_KHR) { // swap chain has become incompatible with the surface
		recreateSwapChain();
		return;
	}
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
		throw std::runtime_error("~	failed to acquire swap chain image!");
	}

	// update the uniform buffers
	updateUniformBuffers();

	// 2) execute the command buffer with that image as attachment in the framebuffer
	// submit our command buffer to the queue (see createCommandBuffers() for written commands)
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	VkSemaphore waitSemaphores[] = { imageAvailableSemaphores[currentFrame] };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT }; // don't write colors to the image until its available
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers[imageIndex];
	VkSemaphore signalSemaphores[] = { renderFinishedSemaphores[currentFrame] }; // signal once the command buffer has finished execution
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;
	vkResetFences(device, 1, &inFlightFences[currentFrame]); // reset fence
	if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame]) != VK_SUCCESS)
		throw std::runtime_error("~ failed to submit draw command buffer!");

	// 3) return the image to the swap chain for presentation
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;
	VkSwapchainKHR swapChains[] = { swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr; // array of results if using multiple swap chains
	result = vkQueuePresentKHR(presentQueue, &presentInfo);
	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || windowResized) {
		recreateSwapChain();
	}
	else if (result != VK_SUCCESS) {
		throw std::runtime_error("~ failed to present swap chain image!");
	}

	// frames in flight functionality
	currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void Renderer::SetView(glm::mat4 view) { offscreen.UBOdata.view = view; }

