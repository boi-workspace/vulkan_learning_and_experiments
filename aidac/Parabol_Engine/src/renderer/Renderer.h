#pragma once

/*
Note that the coordinate system has z being the up axis (like blender and engineering cad)
*/

#include <vulkan/vulkan.h>
#include <glm/glm.hpp> // vector/matrix linear algebra
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES

#include "interface/Interface.h"
#include "model/Model.h"
#include "model/Vertex.h"
#include "config/pbModels.h"

#include <vector>
#include <set>
#include <array>
#include <optional>
#define _USE_MATH_DEFINES
#include <math.h>

// ---------- CONSTS ---------- //

const int MAX_FRAMES_IN_FLIGHT = 1; // frames that can be processed concurrently

// ---------- STRUCTS ---------- //

struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily; // family supports drawing commands
	std::optional<uint32_t> presentFamily; // family supports surface commands

	bool isComplete() {
		return graphicsFamily.has_value() && presentFamily.has_value();
	}
};

struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

// ---------- HELLO TRIANGLE APPLICATION ---------- //

class Renderer {
public:
	void Init(Interface *pbInterface);
	void InitUniformBuffer(glm::mat4 view);

	void DrawFrame(bool windowResized);
	void SetView(glm::mat4 view);

	void Cleanup();

private:
	Interface *pbInterface;

	Model models[NUM_MODELS];

	struct Buffer {
		VkBuffer buffer;
		VkDeviceMemory memory;

		void Destroy(VkDevice device) {
			vkDestroyBuffer(device, buffer, nullptr);
			vkFreeMemory(device, memory, nullptr);
		}
	};

	Buffer vertexBuffer, indexBuffer, instanceBuffer;

	// subpass 0
	struct {
		VkPipelineLayout pipelineLayout;
		VkPipeline pipeline;

		VkDescriptorSetLayout descriptorSetLayout;
		std::array<VkDescriptorSet, NUM_MODELS> descriptorSets;

		Buffer UBO;
		struct {
			glm::mat4 view;
			glm::mat4 proj;
		} UBOdata;

		VkSampler sampler;

		enum Bindings {
			UBO_BINDING,
			TEXTURE_BINDING,
			SAMPLER_BINDING
		};
		enum Locations {
			ALBEDO_LOCATION,
			POSITION_LOCATION,
			NORMAL_LOCATION
		};
	} offscreen;

	// subpass 1
	struct {
		VkPipelineLayout pipelineLayout;
		VkPipeline pipeline;

		VkDescriptorSetLayout descriptorSetLayout;
		VkDescriptorSet descriptorSet;

		Buffer UBO;

		enum Bindings {
			ALBEDO_BINDING,
			POSITION_BINDING,
			NORMAL_BINDING,
			UBO_BINDING
		};
	} composition;

	struct Attachment {
		VkImage image;
		VkDeviceMemory memory;
		VkImageView imageView;
		VkFormat format;

		void destroy(VkDevice device) {
			vkDestroyImageView(device, imageView, nullptr);
			vkDestroyImage(device, image, nullptr);
			vkFreeMemory(device, memory, nullptr);
		}
	};
	Attachment albedoAttachment, positionAttachment, normalAttachment, depthAttachment;

	enum struct AttachmentIndices {
		COMPOSITION,
		ALBEDO,
		POSITION,
		NORMAL,
		DEPTH,
		COUNT
	};

	VkInstance instance;
	VkDebugUtilsMessengerEXT debugMessenger; // the debug util extension object
	
	VkSurfaceKHR surface; // a surface allows you to put stuff on the screen
	VkQueue presentQueue;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE; // (implicity cleaned up with instance)
	VkDevice device; // logical device
	VkQueue graphicsQueue; // (implicitly cleaned up with device)

	// the swap chain is a framebuffer infrastructure - a queue of images waiting to be presented to the screen. syncs with the screen's refresh rate
	VkSwapchainKHR swapChain;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;
	std::vector<VkImage> swapChainImages; // (implicitly cleaned up with swap chain) (size usually 3)
	std::vector<VkImageView> swapChainImageViews;
	std::vector<VkFramebuffer> swapChainFramebuffers;

	VkRenderPass renderPass;

	VkCommandPool commandPool;
	std::vector<VkCommandBuffer> commandBuffers;

	std::vector<VkSemaphore> imageAvailableSemaphores; // for GPU synchronisation
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inFlightFences; // for CPU synchronisation
	size_t currentFrame = 0;

	VkDescriptorPool descriptorPool;

	// seperate function so the swap chain can be recreated
	void cleanupSwapChain();

	// ---------- CREATE INSTANCE ---------- //

	void createInstance();

	// returns a vector of extensions required (by glfw and our debug preferences)
	std::vector<const char*> getRequiredExtensions();

	// print available extensions
	void extensionsCheck();

	// ---------- VALIDATION LAYERS AND DEBUG CALLBACK ---------- //

	// checks if all requested validation layers are available
	bool checkValidationLayerSupport();

	// set up debug util funcionality (if in debug mode)
	void setupDebugMessenger();

	// called by the debug report extension to handle error information produced by active layers
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallbackHandle(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData);

	// ---------- WINDOW SURFACE ---------- //

	// use glfw to create a surface (windows, linux and macOS)
	void createSurface();

	// ---------- PHYSICAL DEVICES AND QUEUE FAMILIES ---------- //

	void pickPhysicalDevice();

	bool isDeviceSuitable(VkPhysicalDevice device);

	// analyse available queue families
	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

	// ---------- SWAP CHAIN ---------- //

	// checks that the device extensions specified at the top are available (includes swap chain extension)
	bool checkDeviceExtensionSupport(VkPhysicalDevice device);

	// get swap chain support details
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice physicalDevice);

	// 1) choose a surface format
	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &availableFormats);

	// 2) choose a presentation mode
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes);

	// 3) define swap extent
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities);

	// create a swap chain using the above configuration funcions
	void createSwapChain();

	// create image views
	void createImageViews();

	// in case the surface changes (e.g. window resize)
	void recreateSwapChain();

	// ---------- LOGICAL DEVICE AND QUEUES ---------- //

	// enable GPU features
	void createLogicalDevice();

	// ---------- GRAPHICS PIPELINE SETUP ---------- //

	void createGraphicsPipelines();

	// binary data loader
	static std::vector<char> readFile(const std::string& filename);

	// wrap SPIR-V code in a shader object
	VkShaderModule createShaderModule(const std::vector<char>& code);

	// ---------- FRAMEBUFFERS ---------- //

	void createFramebuffers();

	void createRenderPass();
	void createAttachment(Attachment& attachment, VkFormat format, VkImageUsageFlags usage);

	// ---------- VERTEX AND INDEX BUFFERS ---------- //

	// load vertices and indices from an external model
	void loadModels();

	// index buffer - an array of pointers into the vertex buffer, allowing vertices to be shared by multiple polygons
	void createGraphicsBuffers();
	void createGraphicsBuffer(VkDeviceSize bufferSize, const void* src, Buffer &buffer, VkBufferUsageFlagBits bufferUsage);

	// creates a buffer and binds memory to it
	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory);

	// used to transfer data from the staging buffer to the vertex buffer (as part of CPU to GPU data transfer)
	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

	VkCommandBuffer beginSingleTimeCommands();

	void endSingleTimeCommands(VkCommandBuffer commandBuffer);

	// find the type of memory to allocate
	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

	// ---------- COMMAND BUFFERS ---------- //

	void createCommandPool();

	void createCommandBuffers();

	// ---------- UNIFORM BUFFERS ---------- //

	// need multiple buffers because there are multiple command buffers (corresponding to the swap chain images)
	void createUniformBuffers();

	void updateUniformBuffers();

	// ---------- DESCRIPTOR CREATION ---------- //

	// a descriptor is a way for shaders to freely access resources like buffers (e.g. perspective transforms) and images (e.g. textures)
	// the layout describes the types of descriptors that can be bound for pipeline creation
	void createDescriptorSetLayout();

	// the pool is used to allocate descriptor sets (of some layout) for use in a shader
	void createDescriptorPool();

	// a descriptor set	specifies the actual buffer or image resources that will be bound to the descriptors (like the frambuffer)
	// here we bind the actual resources to the descriptor
	void createDescriptorSets();

	// ---------- SEMAPHORES AND FENCES ---------- //

	// semaphores are used to synch up those cheeky command queue operations
	// fences are used to sync up application code
	void createSyncObjects();

	// ---------- TEXTURE MAPPING ---------- //

	void createTextureImages();

	void createImage(uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples, VkFormat format, VkImageTiling tiling,
		VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory);

	// transfer an image between layouts so we can record and execute vkCmdCopyBufferToImage
	void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels);

	void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);

	void createTextureImageViews();

	// used by both createTextureImageView and createImageViews
	VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels);

	// sampler is used to read	colors from the texture in the shader - has all sorts of cool texture processes!
	void createTextureSampler();

	// generates mipmaps of a texture vkimage
	void generateMipmaps(VkImage image, VkFormat imageFormat, int32_t texWidth, int32_t texHeight, uint32_t mipLevels);

	// ---------- DEPTH BUFFERING ---------- //

	void createDepthResources();

	bool hasStencilComponent(VkFormat format);

	VkFormat findDepthFormat();

	VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

	// ---------- MULTISAMPLING ---------- //

	// multisampling anti-aliasing
	VkSampleCountFlagBits getMinUsableSampleCount();
};