#pragma once

#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

class Log {
public:
	static void Init();
	inline static std::shared_ptr<spdlog::logger>& Log::GetPBLogger() { return s_PBLogger; }
private:
	static std::shared_ptr<spdlog::logger> s_PBLogger;
};

// log macros
#define PB_TRACE(...)	Log::GetPBLogger()->trace(__VA_ARGS__)
#define PB_INFO(...)	Log::GetPBLogger()->info(__VA_ARGS__)
#define PB_WARN(...)	Log::GetPBLogger()->warn(__VA_ARGS__)
#define PB_ERROR(...)	Log::GetPBLogger()->error(__VA_ARGS__)
#define PB_FATAL(...)	Log::GetPBLogger()->fatal(__VA_ARGS__)