
#include "Parabol.h"
#include "logging/Log.h"
#include "renderer/Renderer.h"

#define GLM_FORCE_RADIANS // ensures glm functions use radians
#define GLM_FORCE_DEPTH_ZERO_TO_ONE // ensures perspective projection matrix uses vulkan range [0,1]
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/rotate_vector.hpp>

#include <iostream>
#include <iomanip>

int main() {

	Parabol engine;
	try {
		engine.Run();
	} catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		//system("pause");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

	// ---------- PARABOL ENGINE CLASS ---------- //

void Parabol::Run() {
	// initalize Parabol engine
	pbInit();

	// enter loop
	pbLoop();

	// clean up
	pbCleanup();
}

void Parabol::pbInit() {

	// initialize logging
	Log::Init();
	PB_INFO("Logger initalized");

	// initialize interface
	pbInterface.Init(this);
	PB_INFO("window interface initalized");

	// initialize vulkan renderer
	pbRenderer.Init(&pbInterface);
	pbRenderer.InitUniformBuffer(glm::lookAt(viewerPosition, viewerPosition + viewerDirection, viewerUp));
	PB_INFO("vulkan renderer initalized");
}

void Parabol::pbCleanup() {
	PB_INFO("beginning parabol cleanup");

	// clean up vulkan renderer
	pbRenderer.Cleanup();
	PB_INFO("renderer instance destroyed");

	// clean up interface
	pbInterface.Cleanup();
	PB_INFO("window interface destroyed");
}

void Parabol::pbLoop() {
	while (!pbInterface.WindowCloseCheck()) {
		pbInterface.PollEvents();

		keyInputsPrev = keyInputs;
		keyInputs = pbInterface.GetKeyInputs();
		if (keyInputs & PB_INPUTS::ESC) break;
		pbProcessInputs();

		pbRenderer.DrawFrame(windowResized);
		windowResized = false;
	}
}

void Parabol::pbProcessInputs() {
	static auto timeStart = high_resolution_clock::now();
	time_point timeCurrent = high_resolution_clock::now();
	float timeTotal = duration<float, seconds::period>(timeCurrent - timeStart).count();
	float timeDif = duration<float, seconds::period>(timeCurrent - timePrev).count();
	timePrev = high_resolution_clock::now();

	// viewer direction
	double mousePosChange[2];
	pbInterface.GetMouseChange(&mousePosChange[0], &mousePosChange[1]);

	float viewAngleHoriz = (float)mousePosChange[0] * radiansPerMousePosHoriz;
	viewerDirection = glm::rotate(viewerDirection, viewAngleHoriz, viewerUp);
	viewerCross = glm::cross(viewerUp, viewerDirection);

	float viewAngleVert = (float)mousePosChange[1] * radiansPerMousePosVert;
	viewerDirection = glm::rotate(viewerDirection, viewAngleVert, viewerCross);
	viewerUp = glm::cross(viewerDirection, viewerCross);

	// viewer position
	if ((bool)(keyInputs & PB_INPUTS::FORWARD) != (bool)(keyInputs & PB_INPUTS::BACKWARD)) {
		viewerPosition += viewerDirection * glm::vec3(keyInputs & PB_INPUTS::FORWARD ? forwardSpeed * timeDif : -backSpeed * timeDif);
	} if ((bool)(keyInputs & PB_INPUTS::LEFT) != (bool)(keyInputs & PB_INPUTS::RIGHT)) {
		viewerPosition += viewerCross * glm::vec3(keyInputs & PB_INPUTS::LEFT ? strafeSpeed * timeDif : -strafeSpeed * timeDif);
	} if ((bool)(keyInputs & PB_INPUTS::UP) != (bool)(keyInputs & PB_INPUTS::DOWN)) {
		viewerPosition += viewerUp * glm::vec3(keyInputs & PB_INPUTS::UP ? strafeSpeed * timeDif : -strafeSpeed * timeDif);
	}

	if ((bool)(keyInputs & PB_INPUTS::ROTATEL) != (bool)(keyInputs & PB_INPUTS::ROTATER)) {
		float viewAngleFront = radiansPerSecondFront * timeDif * (keyInputs & PB_INPUTS::ROTATER ? 1 : -1);
		viewerUp = glm::rotate(viewerUp, viewAngleFront, viewerDirection);
		viewerCross = glm::cross(viewerUp, viewerDirection);
	}

	pbRenderer.SetView(glm::lookAt(viewerPosition, viewerPosition + viewerDirection, viewerUp));
	//pbRenderer.SetLight(glm::vec3(5 * cos(M_PI * timeTotal * 0.2), 5 * sin(M_PI * timeTotal * 0.2), 10));
}

void Parabol::printViewData() {
	static const int width = 7;
	static const int prec = 2;
	std::cout << std::fixed << std::setw(width) << std::showpoint << std::setprecision(prec) << viewerDirection.x << std::setw(width) << std::setprecision(prec) << viewerDirection.y << std::setw(width) << std::setprecision(prec) << viewerDirection.z << " :: "
		<< std::setw(width) << std::setprecision(prec) << viewerCross.x << std::setw(width) << std::setprecision(prec) << viewerCross.y << std::setw(width) << std::setprecision(prec) << viewerCross.z << " :: "
		<< std::setw(width) << std::setprecision(prec) << viewerUp.x << std::setw(width) << std::setprecision(prec) << viewerUp.y << std::setw(width) << std::setprecision(prec) << viewerUp.z << std::endl;
}

void Parabol::windowResizeCallback(GLFWwindow *window, int width, int height) {
	Parabol *application = reinterpret_cast<Parabol *>(glfwGetWindowUserPointer(window));
	application->windowResized = true;
}