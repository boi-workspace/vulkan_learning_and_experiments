#pragma once

#include "config/version_number.h"
#include <string>

int main();
void convertObj(std::string inPath, std::string outPath);
void loadObj(std::string filepath, std::vector<Model::Vertex>& vertices, std::vector<uint32_t>& indices);