// TODO integrate cedai fbx loader

#include "Model_Loader.h"
#include "..\..\Parabol_Engine\src\model\Model.h"
#include "..\..\Parabol_Engine\src\model\LightSource.h"

#define GLM_FORCE_DEPTH_ZERO_TO_ONE // ensures perspective projection matrix uses vulkan range [0,1]
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp> // hash funtions for the glm types

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h> // obj loader

#include <fstream>
#include <iostream>
#include <unordered_map>
#include <array>

// FNV hash functions

namespace std {
	template<> struct hash<Model::Vertex> {
		size_t operator()(Model::Vertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.normal) << 1)) >> 1) ^
				(hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}

namespace std {
	template<> struct hash<LightSource::Vertex> {
		size_t operator()(LightSource::Vertex const& vertex) const {
			return hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.normal) << 1);
		}
	};
}
// ---------- CONFIG ---------- //

const std::string _inFolder = "../../art_dev/convert_path/";
const std::string _outFolder = "../assets/models/";

const std::array<std::string, 2> _modelFiles = {
	"donut_tutorial",
	"chalet"
};
const std::array<std::string, 0> _lightSourceFiles;

// ---------- FUNCTIONS ---------- //

int main() {

	for (int i = 0; i < _modelFiles.size(); i++) {
		// parse obj and write to binary file
		try {
			convertObj(_inFolder + _modelFiles[i], _outFolder + _modelFiles[i]);
		} catch (const std::exception& e) {
			std::cerr << e.what() << std::endl;
			return EXIT_FAILURE;
		}
	}

	std::cout << "Conversion(s) successful!";

	return EXIT_SUCCESS;
}

// TODO pass in pointer to vertex struct?
void convertObj(std::string inPath, std::string outPath) {
	std::cout << "Begin parsing of " + inPath + ".obj" << std::endl;
	std::vector<Model::Vertex> vertices;
	std::vector<uint32_t> indices;

	// parse model data
	loadObj(inPath + ".obj", vertices, indices);
	std::cout << "Begin writing to " + outPath + ".bin" << std::endl;

	// open
	std::ofstream output(outPath + ".bin", std::ios::binary);

	// write version number
	uint16_t versionNumber = _MODEL_VERSION_NUMBER;
	output.write((char*)&versionNumber, sizeof(versionNumber));

	// write mesh type
	uint16_t meshType = _MESH_TYPE::_MODEL;
	output.write((char*)&meshType, sizeof(meshType));

	// write data size
	uint32_t num_vertices = vertices.size();
	uint32_t num_indices = indices.size();
	output.write((char*)&num_vertices, sizeof(uint32_t));
	output.write((char*)&num_indices, sizeof(uint32_t));

	// write model data
	output.write((char*)&vertices[0], sizeof(Model::Vertex)*num_vertices);
	output.write((char*)&indices[0], sizeof(uint32_t)*num_indices);

	output.close();
	std::cout << "num vertices: " << vertices.size() << " num indices: " << indices.size() << std::endl;
}

void loadObj(std::string filepath, std::vector<Model::Vertex> &vertices, std::vector<uint32_t> &indices) {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;
	std::unordered_map<Model::Vertex, uint32_t> uniqueVertices = {};

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filepath.c_str()))
		throw std::runtime_error(warn + err);

	// combine all shapes into single model
	for (const auto &shape : shapes) {
		for (const auto &index : shape.mesh.indices) { // for format see https://github.com/syoyo/tinyobjloader
			Model::Vertex vertex = {};
			vertex.pos = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};
			vertex.texCoord = {
						attrib.texcoords[2 * index.texcoord_index + 0],
				 1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
			};
			vertex.normal = {
				attrib.normals[3 * index.normal_index + 0],
				attrib.normals[3 * index.normal_index + 1],
				attrib.normals[3 * index.normal_index + 2]
			};

			if (uniqueVertices.count(vertex) == 0) { // if we don't have vertex in uniqueVertices
				uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size()); // save index
				vertices.push_back(vertex); // add vertex
			}
			indices.push_back(uniqueVertices[vertex]);
		}
	}
}