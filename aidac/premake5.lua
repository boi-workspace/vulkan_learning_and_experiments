workspace "Aidac"
	architecture "x64" -- no 32 bit support

	configurations {
		"Debug", -- debugging
		"Release", -- optimizations and messages
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "Parabol_Engine" -- game engine
	location "Parabol_Engine"
	kind "ConsoleApp"
	language "C++"

	targetdir ("Parabol_Engine/bin/" .. outputdir)	-- binaries
	objdir ("Parabol_Engine/bin-int/" .. outputdir)	-- intermediate files

	files
	{
		"Parabol_Engine/src/**.h",
		"Parabol_Engine/src/**.cpp",
		"Parabol_Engine/assets/shaders/*.vert",
		"Parabol_Engine/assets/shaders/*.frag"
	}

	includedirs
	{
		"Parabol_Engine/src",
		"Model_Loader/src/config",				-- model reader version number
		"vendor/spdlog/include",				-- spdlog
		"vendor/glm",							-- glm
		"vendor/glfw-3.2.1.bin.WIN64/include",	-- glfw
		"vendor/stb"							-- stb
	}

	libdirs
	{
		"vendor/glfw-3.2.1.bin.WIN64/lib-vc2015"	-- glfw
	}

	links
	{
		"vulkan-1",
		"glfw3"
	}

	filter "system:windows"
		cppdialect "C++17" -- note we may need specific compile flag for other systems
		systemversion "latest"

		includedirs
		{
			"$(VK_SDK_PATH)/Include"
		}

		libdirs
		{
			"$(VK_SDK_PATH)/Lib"
		}

		defines
		{
			"PB_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines "PB_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "PB_RELEASE"
		optimize "On"

project "Model_Loader"
	location "Model_Loader"
	kind "ConsoleApp"
	language "C++"

	targetdir ("Model_Loader/bin/" .. outputdir)	-- binaries
	objdir ("Model_Loader/bin-int/" .. outputdir)	-- intermediate files

	files
	{
		"Model_Loader/src/**.h",		-- all headers
		"Model_Loader/src/**.cpp"	-- all source files
	}

	includedirs
	{
		"Parabol_Engine/src/renderer/config",	-- vertex struct
		"vendor/glm",							-- glm
		"vendor/tinyobjloader"					-- tinyobjloader
	}

	filter "system:windows"
		cppdialect "C++17" -- note we may need specific compile flag for other systems
		systemversion "latest"

		defines
		{
			"PB_PLATFORM_WINDOWS"
		}
